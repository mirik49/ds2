const next = require('next');
const cookieParser = require('cookie-parser');
const port = parseInt(process.env.PORT, 10) || 3001;
const dev = process.env.NODE_ENV !== 'production';
const app = next({dev});
const handle = app.getRequestHandler();
const express = require("express");
const compression = require('compression');

app.prepare().then(() => {
    const server = express();
    server.use(cookieParser());
    server.use(compression());

    server.get('*', (req, res) => {
        return handle(req, res);

    });

    server.listen(port, err => {
        if (err) throw err;
        console.log(`> Ready on http://localhost:${port}`)
    })
});
