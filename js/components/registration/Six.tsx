import React from "react";
import Input from "../pieces/Input";
import Button from "../pieces/Button";
import "../../../scss/components/registration/step6.scss";
import RegPages from "../../layouts/RegPage";

export default function Six(props) {

    const regData = props.registration.regStep6Reducer;
    const code = props.registration.regStep5Reducer.stepFiveDataReturn.code;
    // const photoWithCodeUrl = regData.photoWithCodeUrl;
    const photoWithCode = regData.photoWithCode;
    const photoPassport = regData.photoPassport;
    const photoPassportReg = regData.photoPassportReg;
    const errors = regData.step6Errors;

    return (
        <RegPages>
            <section className="registration-step6">
                <div className="registration-step6__wrapper">
                    <h1 className="registration-step6__title">Сделайте и прикрепите фотографии</h1>
                    <div className="registration-step6__form">
                        <h2 className="registration-step6__form-title">
                            Вам необходимо написать код на бумаге и сфотографироваться так, что бы было хорошо видно
                            лицо и
                            код
                        </h2>
                        <p className="registration-step6__form-code">{code}125 455</p>
                        <p className="registration-step6__form-sample-photo">Сделайте фото с этим кодом (
                            <a className="registration-step6__form-sample-link">
                                пример фото
                            </a>
                            )
                        </p>
                        <div className="registration-step6__form-wrapper">
                            <Input
                                onDrop={e => props.setUploadPhotoWithCode(e)}
                                type="file"
                                id="photoWithCode"
                                className="registration-step6__form"
                                onChange={(e) => props.setUploadPhotoWithCode(e)}
                                error={errors.photoWithCode}
                                onFocus={() => props.clearError("photoWithCode")}
                                labelText="Фотография с кодом"
                                element={photoWithCode ?
                                    <img className="registration-step6__form-img-loaded" src={photoWithCode}
                                         alt={"photoWithCode"}/> : ""}
                            />

                            <Input
                                type="file"
                                id="photoPassport"
                                className="registration-step6__form"
                                labelText="Фотография пасспорта"
                                onDrop={e => props.setUploadPhotoPassport(e)}
                                onChange={(e) => props.setUploadPhotoPassport(e)}
                                error={errors.photoPassport}
                                onFocus={() => props.clearError("photoPassport")}
                                element={photoPassport ?
                                    <img className="registration-step6__form-img-loaded" src={photoPassport}
                                         alt={"photoPassport"}/> : ""}
                            />
                            <Input
                                onDrop={e => props.setUploadPhotoPassportReg(e)}
                                type="file"
                                id="photoPassportReg"
                                className="registration-step6__form"
                                onChange={(e) => props.setUploadPhotoPassportReg(e)}
                                error={errors.photoPassportReg}
                                onFocus={() => props.clearError("photoPassportReg")}
                                labelText="Фотография регистрации"
                                element={photoPassportReg ?
                                    <img className="registration-step6__form-img-loaded" src={photoPassportReg}
                                         alt={"photoPassportReg"}/> : ""}
                            />
                        </div>
                        <Button
                            text="Далее"
                            className="registration-step6__btn-submit"
                            onClick={() => props.submitStepSix(props.history)}
                        />
                    </div>
                </div>
            </section>
        </RegPages>
    )
}
