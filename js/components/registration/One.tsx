import Input from "../pieces/Input";
import Button from "../pieces/Button";
// import {renderTextInput} from "../../helpers/helpers";
import "../../../scss/components/registration/step1.scss";
import RegPages from "../../layouts/RegPage";
import React, {useEffect} from "react";

export default function One(props) {


    useEffect(() => {
        document.addEventListener('keydown', function (e) {
            if (e.keyCode === 113) {
                props.runTest();
            }
        });
    });
    console.log(props);
    const regData = props.registration.regStep1Reducer;
    const goTest = regData.goTest;
    const testData = regData.testData;
    const errors = regData.step1Errors;
    return (
        <RegPages>
            <section className="registration-step1">
                <div className="registration-step1__wrapper">
                    <h1 className="registration-step1__title">Предварительная заявка</h1>
                    <div className="registration-step1__form">
                        <h2 className="registration-step1__form-title">
                            Заполните контактные данные
                        </h2>
                        <Input
                            type="text"
                            className="registration-step1__form"
                            id="lastName"
                            labelText="Фамилия"
                            value={goTest ? testData.lastName : regData.lastName}
                            error={errors.lastName}
                            onFocus={() => props.clearError("lastName")}
                            onChange={(e) => props.setSecondName(e.target.value)}
                        />
                        <Input
                            type="text"
                            className="registration-step1__form"
                            id="firstName"
                            labelText="Имя"
                            value={goTest ? testData.firstName : regData.firstName}
                            error={errors.firstName}
                            onFocus={() => props.clearError("firstName")}
                            onChange={(e) => props.setName(e.target.value)}
                        />
                        <Input
                            type="text"
                            className="registration-step1__form"
                            id="middleName"
                            labelText="Отчество"
                            value={goTest ? testData.middleName : regData.middleName}
                            error={errors.middleName}
                            onFocus={() => props.clearError("middleName")}
                            onChange={(e) => props.setPatronymic(e.target.value)}
                        />
                        <Input
                            type="text"
                            className="registration-step1__form"
                            id="birthDate"
                            labelText="Дата рождения"
                            value={goTest ? testData.birthDate : regData.birthDate}
                            error={errors.birthDate}
                            onFocus={() => props.clearError("birthDate")}
                            onChange={(e) => props.setBirthDate(e.target.value)}
                        />
                        <div className="registration-step1__form-gender-wrapper">
                            <Input
                                type="radio"
                                id="male"
                                name="gender"
                                value="1"
                                className="registration-step1__form"
                                checked="checked"
                                onChange={(e) => props.setGender(e.target.value)}
                                labelText="Мужчина"
                            />
                            <Input
                                type="radio"
                                id="female"
                                name="gender"
                                value="2"
                                className="registration-step1__form"
                                onChange={(e) => props.setGender(e.target.value)}
                                labelText="Женщина"
                            />
                        </div>
                        <Input
                            type="text"
                            className="registration-step1__form"
                            id="phone"
                            labelText="+7 ("
                            value={goTest ? testData.phone : regData.phone}
                            error={errors.phone}
                            onFocus={() => props.clearError("phone")}
                            onChange={(e) => props.setPhone(e.target.value)}
                            mask={"+7 (999) 999 99 99"}
                        />

                        <Button
                            text="Далее"
                            className="registration-step1__btn-submit"
                            id="step1"
                            onClick={() => props.submitStepOne()}
                        />

                    </div>
                    <div className="registration-step1__checkbox-wrapper">
                        <Input
                            type="checkbox"
                            id="subscribe"
                            checked={regData.subscribe}
                            labelText="Подписаться на рассылку"
                            onChagne={props.setSubscribe}
                            className="registration-step1__form"
                        />
                        <Input
                            type="checkbox"
                            id="ConPersonData"
                            checked={regData.consentPersonalData}
                            labelText="Согласие на обработку персональных данных и передачу третьим лицам"
                            onChange={props.setConsentPersonalData}
                            className="registration-step1__form"
                        />
                    </div>
                </div>
            </section>
        </RegPages>
    )
}
