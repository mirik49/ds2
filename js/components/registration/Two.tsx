import Input from "../pieces/Input";
import Button from "../pieces/Button";
import React, {useEffect} from "react";
import "../../../scss/components/registration/step2.scss";
import RegPages from "../../layouts/RegPage";

export default function Step2(props) {
    useEffect(() => {
        document.addEventListener('keydown', function (e) {
            if (e.keyCode === 113) {
                props.runTest();
            }
        });
    });


    const dopAddress = () => {
        return (
            <section>
                <Input
                    labelText="Область"
                    type="text"
                    className="registration-step2__form"
                    onChange={(e) => {
                        props.setActRegion(e.target.value);
                        props.getRegion(e.target.value);
                    }}
                    showHint={regData.showRegionHint}
                    value={goTest ? testData.addressAct.region : regData.addressAct.region}
                    data={region}
                    dataAtr={"region_fias_id"}
                    onClickData={(e) => {
                        props.setSelectedActRegion(e.currentTarget)
                    }}
                />

                <Input
                    type="text"
                    className="registration-step2__form"
                    labelText="Район"
                    showHint={regData.showAreaHint}
                    value={goTest ? testData.addressAct.area : regData.addressAct.area}
                    onChange={(e) => {
                        props.setActArea(e.target.value);
                        props.getArea(e.target.value);

                    }}
                    dataAtr={"area_fias_id"}
                    data={area}
                    onClickData={(e) => {
                        props.setSelectedActArea(e.currentTarget)
                    }}
                />
                <Input
                    type="text"
                    className="registration-step2__form"
                    labelText="Город"
                    value={goTest ? testData.addressAct.city : regData.addressAct.city}
                    onChange={(e) => {
                        props.setActCity(e.target.value);
                        props.getCity(e.target.value);
                    }}
                    showHint={regData.showCityHint}
                    data={city}
                    dataAtr={"city_fias_id, postal_code"}
                    onClickData={(e) => {
                        props.setSelectedActCity(e.currentTarget)
                    }}
                    onFocus={() => props.clearError("addressAct.city")}
                    error={errors["addressAct.city"] ? ["Поле город обязательно для заполнения когда не указан населенный пункт"] : ""}
                />
                <Input
                    type="text"
                    className="registration-step2__form"
                    labelText="Населенный пункт"
                    value={goTest ? testData.addressAct.settlement : regData.addressAct.settlement}
                    onChange={(e) => {
                        props.setActSettlement(e.target.value);
                        props.getSettlement(e.target.value);
                    }}
                    showHint={regData.showSettlementHint}
                    data={settlement}
                    dataAtr={"settlement_fias_id"}
                    onClickData={(e) => {
                        props.setSelectedActSettlement(e.currentTarget)
                    }}
                    onFocus={() => props.clearError("addressAct.settlement")}
                    error={errors["addressAct.settlement"] ? ["Поле населенный пункт обязательно для заполнения когда не указан город"] : ""}
                />
                <Input
                    type="text"
                    className="registration-step2__form"
                    labelText="Улица"
                    value={goTest ? testData.addressAct.street : regData.addressAct.street}
                    onChange={(e) => {
                        props.setActStreet(e.target.value);
                        props.getStreet(e.target.value);
                    }}
                    showHint={regData.showStreetHint}
                    data={street}
                    dataAtr={"street_fias_id"}
                    onClickData={(e) => {
                        props.setSelectedActStreet(e.currentTarget)
                    }}
                    onFocus={() => props.clearError("addressAct.street")}
                    error={errors["addressAct.street"] ? ["Поле улица обязательно для заполнения"] : ""}
                />
                <Input
                    type="text"
                    className="registration-step2__form"
                    labelText="Дом"
                    value={goTest ? testData.addressAct.houseNumber : regData.addressAct.houseNumber}
                    onChange={(e) => {
                        props.setActHouseNumber(e.target.value);
                        props.getHouseNumber(e.target.value);
                    }}
                    showHint={regData.showHouseNumberHint}
                    data={house}
                    onClickData={(e) => {
                        props.setSelectedActHouseNumber(e.currentTarget)
                    }}
                />
                <Input
                    type="text"
                    className="registration-step2__form"
                    labelText="Корпус"
                    value={goTest ? testData.addressAct.blockNumber : regData.addressAct.blockNumber}
                    onChange={(e) => props.setActBlockNumber(e.target.value)}
                />
                <Input
                    type="text"
                    className="registration-step2__form"
                    labelText="Кв"
                    value={goTest ? testData.addressAct.flatNumber : regData.addressAct.flatNumber}
                    onChange={(e) => props.setActFlatNumber(e.target.value)}
                />
                <Input
                    type="text"
                    className="registration-step2__form"
                    labelText="Почтовый индекс"
                    value={goTest ? testData.addressAct.zip : regData.addressAct.postalCode}
                    onChange={(e) => props.setActPostalCode(e.target.value)}
                    onFocus={() => props.clearError("addressAct.zip")}
                    error={errors["addressAct.zip"] ? ["Почтовый код обязательно для заполнения"] : ""}
                />
                <Input
                    type="text"
                    className="registration-step2__form"

                    labelText="Дата начала регистрации"
                    value={goTest ? testData.addressAct.dateRegistration : regData.addressAct.dateRegistration}
                    onChange={(e) => props.setActDateRegistration(e.target.value)}
                />
            </section>
        )
    };

    const regData = props.registration.regStep2Reducer;
    const goTest = regData.goTest;
    const testData = regData.testData;
    const dadata = props.dadata;
    const errors = regData.step2Errors;


    let region = dadata.searchRegionArr ? dadata.searchRegionArr.suggestions : {};
    let area = dadata.searchAreaArr.suggestions ? dadata.searchAreaArr.suggestions : {};
    let city = dadata.searchCityArr.suggestions ? dadata.searchCityArr.suggestions : {};
    let street = dadata.searchStreetArr.suggestions ? dadata.searchStreetArr.suggestions : {};
    let settlement = dadata.searchSettlementArr.suggestions ? dadata.searchSettlementArr.suggestions : {};
    let house = dadata.searchHouseArr.suggestions ? dadata.searchHouseArr.suggestions : {};
    return (
        <RegPages>
            <section className="registration-step2">
                <div className="registration-step2__wrapper">
                    <h1 className="registration-step2__title">Оплата займа</h1>
                    <div className="registration-step2__form">
                        <h2 className="registration-step2__form-title">
                            Заполните паспортные данные
                        </h2>

                        <Input
                            mask="9999 999999"
                            type="text"
                            id="passportData"
                            className="registration-step2__form"
                            labelText="Серия и номер паспорта"
                            value={goTest ? testData.series + " " + testData.number : regData.passportSeries + regData.passportNumber}
                            onChange={(e) => props.setPasportNumber(e.target.value)}
                            onFocus={() => props.clearError("number")}
                            error={errors.number ? ["Серия и номер пасспорта обязательно для заполнения"] : ""}
                        />
                        <Input
                            type="text"
                            id="issuedBy"
                            className="registration-step2__form"
                            labelText="Кем выдан паспорт"
                            value={goTest ? testData.issuedBy : regData.issuedBy}
                            onChange={(e) => props.setIssuedBy(e.target.value)}
                            onFocus={() => props.clearError("issuedBy")}
                            error={errors.issuedBy ? ["Кем выдан паспорт обязательно для заполнения"] : ""}
                        />

                        <Input
                            mask="99 99 9999"
                            type="text"
                            id="issuedDate"
                            className="registration-step2__form"
                            labelText="Дата выдачи паспорта"
                            value={goTest ? testData.issuedDate : regData.issuedDate}
                            onChange={(e) => props.setIssuedDate(e.target.value)}
                            onFocus={() => props.clearError("issuedDate")}
                            error={errors.issuedDate ? ["Дата выдачи пасспорта обязательно для заполнения"] : ""}
                        />
                        <Input
                            mask="999-999"
                            type="text"
                            id="issuedSubDiv"
                            className="registration-step2__form"
                            labelText="Код подразделения"
                            value={goTest ? testData.issuedSubdivision : regData.issuedSubdivision}
                            onChange={(e) => props.setIssuedSubdivision(e.target.value)}
                            onFocus={() => props.clearError("issuedSubdivision")}
                            error={errors.issuedSubdivision ? ["Код подразделения обязательно для заполнения"] : ""}
                        />
                        <Input
                            type="text"
                            id="birthPlace"
                            className="registration-step2__form"
                            labelText="Место рождения"
                            value={goTest ? testData.birthPlace : regData.birthPlace}
                            onChange={(e) => props.setBirthPlace(e.target.value)}
                            onFocus={() => props.clearError("birthPlace")}
                            error={errors.birthPlace ? ["Место рождения обязательно для заполнения"] : ""}
                        />
                        <Input
                            mask="999-999-999 99"
                            type="text"
                            id="snils"
                            className="registration-step2__form"
                            labelText="СНИЛС"
                            value={goTest ? testData.snils : regData.snils}
                            onChange={(e) => props.setSnils(e.target.value)}
                            // error={errors.snils}
                        />
                        <h2 className="registration-step2__form-title">Адресс регистрации</h2>

                        <Input
                            id="region"
                            type="text"
                            labelText="Область"
                            className="registration-step2__form"
                            onChange={(e) => {
                                props.setRegion(props.getRegion(e.target.value));

                            }}
                            showHint={regData.showRegionHint}
                            value={goTest ? testData.addressReg.region :regData.addressReg.region}
                            data={region}
                            dataAtr={"region_fias_id"}
                            onClickData={(e) => {
                                props.setSelectedRegion(e.currentTarget)
                            }}
                            // error={errors.addressReg.region} //необязательный
                        />

                        <Input
                            id="area"
                            type="text"
                            className="registration-step2__form"
                            labelText="Район"
                            showHint={regData.showAreaHint}
                            value={goTest ? testData.addressReg.area : regData.addressReg.area}
                            onChange={(e) => {
                                props.setArea(e.target.value);
                                props.getArea(e.target.value);

                            }}
                            dataAtr={"area_fias_id"}
                            data={area}
                            onClickData={(e) => {
                                props.setSelectedArea(e.currentTarget)
                            }}
                            // error={errors.addressReg.area} //не обязательное поле
                        />
                        <Input
                            id="city"
                            type="text"
                            className="registration-step2__form"
                            labelText="Город"
                            value={goTest ? testData.addressReg.city : regData.addressReg.city}
                            onChange={(e) => {
                                props.setCity(e.target.value);
                                props.getCity(e.target.value);
                            }}
                            showHint={regData.showCityHint}
                            data={city}
                            dataAtr={"city_fias_id, postal_code"}
                            onFocus={() => props.clearError("addressReg.city")}
                            onClickData={(e) => {
                                props.setSelectedCity(e.currentTarget)
                            }}
                            error={errors["addressReg.city"] ? ["Поле город обязательно для заполнения когда не указан населенный пункт"] : ""}
                        />
                        <Input
                            id="settlement"
                            type="text"
                            className="registration-step2__form"
                            labelText="Населенный пункт"
                            value={goTest ? testData.addressReg.settlement : regData.addressReg.settlement}
                            onChange={(e) => {
                                props.setSettlement(e.target.value);
                                props.getSettlement(e.target.value);
                            }}
                            showHint={regData.showSettlementHint}
                            data={settlement}
                            dataAtr={"settlement_fias_id"}
                            onClickData={(e) => {
                                props.setSelectedSettlement(e.currentTarget)
                            }}
                            onFocus={() => props.clearError("addressReg.settlement")}
                            error={errors["addressReg.settlement"] ? ["Поле населенный пункт обязательно для заполнения когда не указан город"] : ""}
                        />
                        <Input
                            id="street"
                            type="text"
                            className="registration-step2__form"
                            labelText="Улица"
                            value={goTest ? testData.addressReg.street : regData.addressReg.street}
                            onChange={(e) => {
                                props.setStreet(e.target.value);
                                props.getStreet(e.target.value);
                            }}
                            showHint={regData.showStreetHint}
                            data={street}
                            dataAtr={"street_fias_id"}
                            onClickData={(e) => {
                                props.setSelectedStreet(e.currentTarget)
                            }}
                            onFocus={() => props.clearError("addressReg.street")}
                            error={errors["addressReg.street"] ? ["Поле улица обязательно для заполнения"] : ""}
                        />
                        <Input
                            id="houseNumber"
                            type="text"
                            className="registration-step2__form"
                            labelText="Дом"
                            value={goTest ? testData.addressReg.houseNumber : regData.houseNumber}
                            onChange={(e) => {
                                props.setHouseNumber(e.target.value);
                                props.getHouseNumber(e.target.value);
                            }}
                            showHint={regData.showHouseNumberHint}
                            data={house}
                            onClickData={(e) => {
                                props.setSelectedHouseNumber(e.currentTarget)
                            }}
                            error={errors["addressReg.houseNumber"]}
                        />
                        <Input
                            id="blockNumber"
                            type="text"
                            className="registration-step2__form"
                            labelText="Корпус"
                            value={goTest ? testData.addressReg.blockNumber : regData.addressReg.blockNumber}
                            onChange={(e) => props.setBlockNumber(e.target.value)}

                        />
                        <Input
                            id="flatNumber"
                            type="text"
                            className="registration-step2__form"
                            labelText="Кв"
                            value={goTest ? testData.addressReg.flatNumber : regData.addressReg.flatNumber}
                            onChange={(e) => props.setFlatNumber(e.target.value)}
                        />
                        <Input
                            id="postalCode"
                            type="text"
                            className="registration-step2__form"
                            labelText="Почтовый индекс"
                            value={goTest ? testData.addressReg.zip : regData.addressReg.postalCode}
                            onChange={(e) => props.setPostalCode(e.target.value)}
                            onFocus={() => props.clearError("addressReg.zip")}
                            error={errors["addressReg.zip"] ? ["Почтовый код обязательно для заполнения"] : ""}
                        />
                        <Input
                            type="text"
                            id="dateRegistration"
                            className="registration-step2__form"
                            labelText="Дата начала регистрации"
                            value={goTest ? testData.addressReg.dateRegistration : regData.addressReg.dateRegistration}
                            onChange={(e) => props.setDateRegistration(e.target.value)}
                        />

                        <h2 className="registration-step2__form-title">Адресс проживания</h2>
                        <Input
                            id="livingByRegAddress"
                            type="checkbox"
                            className="registration-step2__form"
                            labelText="Проживаю по прописке"
                            checked={regData.livingByRegAddress}
                            onChange={() => props.setLivingByRegAddress()}
                        />
                        {regData.livingByRegAddress ? "" : dopAddress()}

                        <Button
                            text="Далее"
                            className="registration-step2__btn-submit"
                            onClick={() => props.submitStepTwo()}
                        />
                    </div>
                </div>
            </section>
        </RegPages>
    )
}
