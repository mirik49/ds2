import React from "react";
import Input from "../pieces/Input";
import Button from "../pieces/Button";
import {CLOC_ICON} from "../../constants/IconConstants";
import "../../../scss/components/registration/final-step.scss";
import RegPages from "../../layouts/RegPage";

export default function FinalStep(props) {
    // useEffect(() => {
    //     document.addEventListener('keydown', function (e) {
    //         if (e.keyCode === 113) {
    //             props.runTestStep5();
    //         }
    //     });
    // });
    const regData = props.registration.regFinalStepReducer;
    const token = props.registration.regStep4Reducer.userAuthToken;
    console.log(token);
    return (
        <RegPages>
            <section className="registration-final-step">
                <div className="registration-final-step__wrapper">
                    <h1 className="registration-final-step__title"> Заявка на рассмотрении</h1>
                    <p className="registration-final-step__image-wrapper">{CLOC_ICON}</p>
                    <p className="registration-final-step__title-text">Обычно это занимает не более 30 минут</p>
                    <div className="registration-final-step__form">
                        <p className="registration-final-step__form-title">Установите пароль для последующих входов</p>

                        <div>
                            <Input
                                id="password"
                                className="registration-final-step__form"
                                type="text"
                                labelText="Пароль"
                                value={regData.password}
                                onChange={(e) => props.setPassword(e.target.value)}
                            />
                            <Input
                                id="confirmPassword"
                                className="registration-final-step__form"
                                type="text"
                                labelText="Подтверждение пароля"
                                value={regData.confirmPassword}
                                onChange={(e) => props.setConfirmPassword(e.target.value)}
                            />
                        </div>
                        <Button
                            className="registration-final-step__form-btn-submit"
                            text={"Далее"}
                            type={"button"}
                            onClick={() => props.submitFinalStep()}
                        />
                    </div>
                </div>
            </section>
        </RegPages>
    )
}
