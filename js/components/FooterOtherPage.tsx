import React from "react";
import {MAIN_LOGO} from "../constants/IconConstants";
// import {Link} from "react-router-dom";
import "../../scss/components/footer-other-page.scss";

export default function FooterOtherPage() {
    return (
        <footer className={"footer-other-page"}>
            <div className={"footer-other-page__block"}>
                <p className={"footer-other-page__logo-wrapper"}>
                    {/*<Link to={"/"}>*/}
                        {MAIN_LOGO}
                    {/*</Link>*/}
                </p>

            </div>
            <div className={"footer-other-page__block"}>
                <p className={"footer-other-page__block-text"}>
                    © 2011-2019 Общество с ограниченной ответственностью "Микрокредитная компания Скорость Финанс"
                </p>
            </div>
        </footer>
    )
}

