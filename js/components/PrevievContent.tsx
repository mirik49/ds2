import React from "react";

import RegPages from "../layouts/RegPage";

export default function PreviewContent() {
    return (
        <RegPages>
            <section className="auth">
                <div className="auth__wrapper">
                    <h1 className="auth__title">Предпросмотр контента</h1>
                </div>
            </section>
        </RegPages>
    )
}
