import React from "react";
import {MAIN_LOGO, MAKE_REQUEST_ICON, PAY_LOAN_ICON, PER_CAB_ICON} from "../constants/IconConstants";
import SiteMenu from "./pieces/SiteMenu";
import Link from "next/link";

// import Link from "next/link";

// interface events extends EventTarget {
//     onClick: () => void,
//     _setOpenMenu: () => void,
//     supportPhone: any,
//
// }

export default function HeaderMenu(props) {
    return (
        <section className="header-menu">
            <div className="header-menu__wrapper">
                <p className="header-menu__logo-wrapper">
                    {MAIN_LOGO}
                </p>
                <ul className="header-menu__user-list">
                    <li className="header-menu__user-item">
                        <Link href="/one">
                            <a onClick={props._setOpenMenu}>
                                <p className="header-menu__user-item-img">
                                    {MAKE_REQUEST_ICON}
                                </p>
                                <p className="header-menu__user-item-text">
                                    Офоримть заявку
                                </p>
                            </a>
                        </Link>
                    </li>
                    <li className="header-menu__user-item">
                        {/*<Link href={"/pay-loan"} onClick={props.setOpenMenu}>*/}
                            <a>
                                <p className="header-menu__user-item-img">
                                    {PAY_LOAN_ICON}
                                </p>
                                <p className="header-menu__user-item-text">
                                    Оплатить займ
                                </p>
                            </a>
                        {/*</Link>*/}
                    </li>
                    <li className="header-menu__user-item">
                        <Link href="/cabinet" >
                            <a onClick={props._setOpenMenu}>
                                <p className="header-menu__user-item-img">
                                    {PER_CAB_ICON}
                                </p>
                                <p className="header-menu__user-item-text">
                                    Личный кабинет
                                </p>
                            </a>
                        </Link>
                    </li>
                </ul>
                <SiteMenu
                    mainBlock="header-menu"
                />
                <div className="header-menu__contact-wrapper">
                    <a href={"tel:" + props.supportPhone}
                       className="header-menu__contact-telephone"
                    >
                        8 (800) 700 74 24
                    </a>
                    <p className="header-menu__contact-text">
                        Бесплатно по всей России
                    </p>
                </div>
            </div>
        </section>
    )
}
