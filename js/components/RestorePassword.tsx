import React from "react";
import Input from "./pieces/Input";
import Button from "./pieces/Button";
import "../../scss/components/restore.scss";
import RegPages from "../layouts/RegPage";

export default function restorePAssword(props) {
    const restoreEnterCodeForm = () => {
        return (
            <React.Fragment>
                <h2 className="restore__form-title">Введите пароль</h2>
                <Input
                    type="text"
                    className="restore__form"
                    labelText="pass"
                    onFocus={() => props.clearError("password")}
                    value={restoreData.restoreCode}
                    onChange={(e) => props.setRestoreCode(e.target.value)}
                    error={restoreData.restoreErrors.password ? restoreData.restoreErrors.password : ""}
                />
                <Button
                    className="restore__btn-submit"
                    text="Войти"
                    onClick={() => props.restoreAuth(props.history)}
                />
            </React.Fragment>
        )
    };

    const restorePasswordForm = () => {
        return (
            <React.Fragment>
                <h2 className="restore__form-title">{restoreData.restoreSuccessMessage}</h2>
                <h2 className="restore__form-title">Введите номер телефона, на который вам будет отправлен код</h2>
                <Input
                    mask="+7(999) 999 99 99"
                    type="text"
                    className="restore__form"
                    labelText="Телефон"
                    onFocus={() => props.clearError("phone")}
                    value={restoreData.restorePhone}
                    onChange={(e) => props.setRestorePhone(e.target.value)}
                    error={typeof restoreData.restoreErrors !== "undefined" ? restoreData.restoreErrors.phone : ""}
                />
                <Button
                    className="restore__btn-submit"
                    text="Войти"
                    onClick={() => props.restorePassword(props.history)}
                />

            </React.Fragment>
        )
    };
    const renderCode = () => {
        const showrestoreEnterCode = restoreData.testRestoreCode;
        return (
            <p> {showrestoreEnterCode}</p>
        )
    };
    const restoreData = props.restoreReducer!==undefined ? props.restoreReducer : "";
    // const errors = restoreData.restoreErrors;
    // const [isShowRestorePassword, onShowRestorePassword] = useState(false);
    // console.log(errors);

    return (
        <RegPages>
            <section className="restore">
                <div className="restore__wrapper">
                    <h1 className="restore__title">Восстановление пароля</h1>
                    <div className="restore__form-wrapper">
                        {process.env.NODE_ENV === "development" ? renderCode() : ""}
                        <h2 className="restore__form-title">{restoreData.restoreMessage}</h2>
                        <div className="restore__form-block-wrapper">
                            {restoreData.restoreSuccess ? restoreEnterCodeForm() : restorePasswordForm()}
                        </div>
                    </div>
                </div>
            </section>
        </RegPages>
    )
}

