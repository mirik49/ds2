import React from 'react';
import {FB_ICON, INST_ICON, MAIN_LOGO, OK_ICON, TW_ICON, VK_ICON} from "../constants/IconConstants";
import Link from "next/link";
import SiteMenu from "./pieces/SiteMenu";
import "../../scss/components/footer.scss";

export default function Footer() {
    // const [showInfo, changePosition] = useState(1);
    // useEffect(() => {
    //     let width = window.innerWidth;
    //     if (width >= 1024) {
    //         changePosition(2);
    //     }
    // });
    const info = () => {
        return (
            <div className="footer__block-info">
                <p className="footer__block-info-text">
                    © 2011-2019
                    Общество с ограниченной ответственностью &laquo;Микрокредитная компания Скорость Финанс&raquo;
                </p>
            </div>
        )
    };
    return (
        <footer className="footer">

            <div className="footer__wrapper">
                <div className="footer__block">
                    <p className="footer__logo-wrapper">
                        <Link href="index">
                            <a>{MAIN_LOGO}
                                {/*index*/}
                            </a>
                        </Link>
                    </p>
                    {/*{showInfo === 2 ? info() : ""}*/}
                    <ul className="footer__social-list">
                        <li className="footer__social-item">
                            <a className="footer__social-link">
                                {VK_ICON}
                            </a>
                        </li>
                        <li className="footer__social-item">
                            <a className="footer__social-link">
                                {FB_ICON}
                            </a>
                        </li>
                        <li className="footer__social-item">
                            <a className="footer__social-link">
                                {OK_ICON}
                            </a>
                        </li>
                        <li className="footer__social-item">
                            <a className="footer__social-link">
                                {INST_ICON}
                            </a>
                        </li>
                        <li className="footer__social-item">
                            <a className="footer__social-link">
                                {TW_ICON}
                            </a>
                        </li>
                    </ul>
                </div>
                <SiteMenu
                    mainBlock="footer"
                />
            </div>
            {/*{showInfo === 1 ? info() : ""}*/}
            {info()}

        </footer>
    )
}



