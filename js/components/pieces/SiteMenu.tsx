import React, {useEffect, useState} from "react";
import PropTypes from 'prop-types';
import "../../../scss/components/header-menu.scss";
import Link from "next/link";

export default function SiteMenu(props) {

    const [isPC, setWidth] = useState(false);
    useEffect(() => {
        let width = window.innerWidth;

        let isPC = width >= 1024 ? true : false;
        setWidth(isPC);

    });

    const [state, setState] = useState({id: ""});

    const renderItem = (data, key) => {
        return (
            <li className={props.mainBlock + "__item"} key={key}>
                <Link href={data[1]}>
                    <a className={props.mainBlock + "__item-link"}
                    >{data[0]}</a>
                </Link>
            </li>
        );
    };

    const aboutCompany = () => {
        return (
            <React.Fragment>
                <ul className={props.mainBlock + "__list-menu"}>
                    {[
                        ["История становления", "link"],
                        ["Достижения и награды", "link"],
                        ["Документация", "link"],
                        ["Благотворительность", "link"],
                        ["Новости", "link"],
                        ["Информация инвесторам", "link"],
                        ["Структура сайта", "/site-structure"],
                    ].map(renderItem)}
                </ul>
            </React.Fragment>
        )
    };

    const clients = () => {
        return (
            <React.Fragment>
                <ul className={props.mainBlock + "__list-menu"}>
                    {[
                        ["Оформить заявку", "link"],
                        ["Личный кабинет", "link"],
                        ["Вопросы и ответы", "link"],
                        ["Блог", "link"],
                        ["Отзывы клиентов", "link"],
                        ["Политика конфиденциальности", "link"],
                        ["Города выдачи", "link"]
                    ].map(renderItem)}
                </ul>
            </React.Fragment>
        )
    };

    const career = () => {
        return (
            <React.Fragment>
                <ul className={props.mainBlock + "__list-menu"}>
                    {[
                        ["Вакансии", "link"],
                        ["Истории успеха", "link"],
                        ["Корпоративный университет", "link"],
                        ["Достижения HR", "link"],
                    ].map(renderItem)}
                </ul>
            </React.Fragment>
        )
    };
    return (
        <>
            <div className={props.mainBlock + "__block-menu"}>
                <h2 className={state.id === "about-company" ? props.mainBlock + "__block-title selected" : props.mainBlock + "__block-title"}
                    tabIndex={0}
                    onClick={() => setState({id: "about-company"})}
                >
                    О компании
                </h2>
                {!isPC ? state.id === "about-company" ? aboutCompany() : "" : aboutCompany()}
            </div>
            <div className={props.mainBlock + "__block-menu"}>
                <h2 className={state.id === "clients" ? props.mainBlock + "__block-title selected" : props.mainBlock + "__block-title"}
                    tabIndex={0}
                    onClick={() => setState({id: "clients"})}
                >
                    Клиентам
                </h2>
                {!isPC ? state.id === "clients" ? clients() : "" : clients()}
            </div>
            <div className={props.mainBlock + "__block-menu"}>
                <h2 className={state.id === "career" ? props.mainBlock + "__block-title selected" : props.mainBlock + "__block-title"}
                    tabIndex={0}
                    onClick={() => setState({id: "career"})}
                >
                    Карьера
                </h2>
                {!isPC ? state.id === "career" ? career() : "" : career()}
            </div>
        </>
    )
}

SiteMenu.propTypes = {
    mainBlock: PropTypes.string
};
