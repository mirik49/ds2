import React,{ Component } from "react";
import {PAY_MAESTRO_ICON, PAY_MASTERCARD_ICON, PAY_MIR_ICON, PAY_VISA_ICON} from "../../constants/Constants";

export default class PaySystem extends Component {
    render() {
        return (
            <React.Fragment>
                <ul className={"pay-system__list"}>
                    <li className={"pay-system__item"}>
                        <p className={"pay-system__image-wrapper"}>
                            {PAY_MIR_ICON}
                        </p>
                    </li>
                    <li className={"pay-system__item"}>
                        <p className={"pay-system__image-wrapper"}>
                            {PAY_MASTERCARD_ICON}
                        </p>
                    </li>
                    <li className={"pay-system__item"}>
                        <p className={"pay-system__image-wrapper"}>
                            {PAY_VISA_ICON}
                        </p>
                    </li>
                    <li className={"pay-system__item"}>
                        <p className={"pay-system__image-wrapper"}>
                            {PAY_MAESTRO_ICON}
                        </p>
                    </li>
                    <li className={"pay-system__item"}>
                        <p className={"pay-system__image-wrapper"}>
                            YANDEX
                        </p>
                    </li>

                </ul>
            </React.Fragment>
        )
    }
}
