import React from "react";
import Button from '../pieces/Button';
import Input from "../pieces/Input";

export default function PayWithoutEntering() {
    return (
        <React.Fragment>
            <section className="pay-without-entering">
                <div className="pay-without-entering__wrapper">
                    <p className="pay-without-entering__image-wrapper">
                        {/*<img src="/static/images/pay-without-entering.jpg" alt="image pay without entering"/>*/}
                        <img src="/static/images/pay-without-entering.webp" alt="image pay without entering"/>
                    </p>
                    <div className="pay-without-entering__form">
                        <h2 className="pay-without-entering__form-title">
                            Оплатите займ без входа в личный кабинет
                        </h2>
                        <Input
                            id="contractNumber"
                            type="text"
                            className="pay-without-entering__form"
                            labelText="Введите № договора"
                        />
                        <Button
                            type="button"
                            className="pay-without-entering__form-btn-pay"
                            text="Оплатить займ"
                        />
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

