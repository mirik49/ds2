import React, {useEffect} from "react";
import Input from "../pieces/Input";
import Slider from 'react-rangeslider';
import EsiaBtn from "./EsiaBtn";
import Link from "next/link";

export default function Calc(props) {
    if (process.browser) {
        useEffect(() => {
            props._getDefaultData();
        },[]);
    }

    const data = props.calc;
    const defaultData = typeof data.defaultCalcData !=="undefined" ? data.defaultCalcData : {};
    return (
        <React.Fragment>
            <section className="calc">
                <h1 className="calc__title">
                    Займ под 1 %
                </h1>
                <p className="calc__title-text">
                    Подайте заявку и получите займ до 1 июля 2019 года и вы получите
                    следующий займ под 1%
                </p>
                <div className={"calc__wrapper"}>
                    <div className="calc__range-slider-wrapper">
                        <p className="calc__range-summ">
                            {data.sum ? data.sum + " " : "1000"}
                            ₽</p>
                        <div className="calc__range-wrapper rangeslider">
                            <Slider
                                value={data.sum ? data.sum + " " : "1000"}
                                min={parseInt(data.defaultSum.min ? data.defaultSum.min: "1000")}
                                max={parseInt(data.defaultSum.max ? data.defaultSum.max: "10000")}
                                step={parseInt(data.defaultSum.step ? data.defaultSum.step: "1000")}
                                orientation="horizontal"
                                onChange={props.setCalcSum}
                                tooltip={false}
                                // labels={ {100: data.defaultSum.min, 0: data.defaultSum.max}}
                            />
                            {/*<Range*/}
                            {/*    className="calc__range-summ-input"*/}
                            {/*    min={props.sumMin}*/}
                            {/*    max={props.sumMax}*/}
                            {/*    step={props.sumStep}*/}
                            {/*    defaultValue={props.defaultSumValue}*/}
                            {/*    setValue={props.getSum}*/}
                            {/*/>*/}
                        </div>
                        <p className="calc__range-summ">
                            {defaultData.defaultPeriod}
                            дней</p>
                        <div className="calc__range-wrapper rangeslider">
                            {/*<Range*/}
                            {/*    className="calc__range-day-input"*/}
                            {/*    min={props.dayMin}*/}
                            {/*    max={props.dayMax}*/}
                            {/*    step={props.dayStep}*/}
                            {/*    defaultValue={props.defaultDayValue}*/}
                            {/*    setValue={props.setDay}*/}
                            {/*/>*/}
                        </div>
                    </div>
                    <Input
                        type="text"
                        id="promocode"
                        className="calc__promocode"
                        labelText="Введите промокод"
                        onChange={props.setPromocode}
                        value={props.promocode}

                    />
                    <ul className="calc__output">
                        <li className="calc__output-item">
                            <p className="calc__output-description">
                                До (включительно)
                            </p>
                            <p className="calc__output-value">
                                {data.returnDate}
                            </p>
                        </li>
                        <li className="calc__output-item">
                            <p className="calc__output-description">
                                Вернуть
                            </p>
                            <p className="calc__output-value">
                                {data.returnSum}
                                ₽
                            </p>
                        </li>
                        {/*//пока что отложено*/}
                        {/*<li className={"calc__output-item"}>*/}
                        {/*    <p className={"calc__output-description"}>*/}
                        {/*        Платеж раз в 2 недели*/}
                        {/*    </p>*/}
                        {/*    <p className={"calc__output-value"}>*/}
                        {/*        {}*/}
                        {/*    </p>*/}
                        {/*</li>*/}
                    </ul>
                    <Link href="/cabinet"><a className="calc__take-loan-btn">Получить дненьги</a></Link>
                    <EsiaBtn/>
                </div>
                <p className="calc__footnote">Выдача займов до 120 000 рублей</p>
            </section>
        </React.Fragment>
    )

}
