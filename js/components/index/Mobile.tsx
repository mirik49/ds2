import React from "react";
import Button from '../pieces/Button';
// import Input from "../pieces/Input.jsx";
// import {withRouter} from 'react-router-dom';

import {
    ANDROID_ICON,
    APP_STORE_ICON,
    // CARD_TRANSFER_ICON,
    // CREDIT_REITING_ICON,
    GOOGLE_PLAY_ICON, IOS_ICON,
    // LOAN_PTS_ICON
} from "../../constants/IconConstants";

export default function Mobile() {

    return (
        <React.Fragment>
            <section className="mobile">
                <h2 className={"mobile__title"}>
                    Мы в <span>смартфоне</span>
                </h2>
                <div className="mobile__wrapper">
                    <div className="mobile__link">
                        <p className="mobile__link-wrapper">
                            {IOS_ICON}
                        </p>
                        <p className="mobile__link-wrapper">
                            {ANDROID_ICON}
                        </p>
                    </div>
                    <div className="mobile__app">
                        <ul className="mobile__app-list-qr">
                            <li className="mobile__app-item-qr">
                                <p className="mibile__app-image-wrapper">

                                </p>
                            </li>
                            <li className="mobile__app-item-qr">
                                <p className="mibile__app-image-wrapper">

                                </p>
                            </li>
                        </ul>
                        <div className="mobile__app-wrapper">
                            <ul className="mobile__app-list">
                                <li className="mobile__app-item">
                                    <p className="mobile__app-image-wrapper">
                                        {GOOGLE_PLAY_ICON}
                                    </p>
                                    <p className={"mobile__app-download"}>
                                        <span className="mobile__app-download-counter">{10504}</span>
                                        <span className="mobile__app-download-text">скачиваний</span>
                                    </p>
                                </li>
                                <li className="mobile__app-item">
                                    <p className="mobile__app-image-wrapper">
                                        {APP_STORE_ICON}
                                    </p>
                                    <p className="mobile__app-download">
                                        <span className="mobile__app-download-counter">{1504}</span>
                                        <span className="mobile__app-download-text">отзывов</span>
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <h3 className={"mobile__app-advantages-title"}>
                            Основные функции
                        </h3>
                        <ul className={"mobile__app-advantages-list"}>
                            <li className={"mobile__app-advantages-item"}>
                                <p className={"mobile__app-advantages-item-text"}>
                                    Уведомление о статусе заявки
                                </p>
                            </li>
                            <li className={"mobile__app-advantages-item"}>
                                <p className={"mobile__app-advantages-item-text"}>
                                    Оформление заявки за 4 минуты
                                </p>
                            </li>
                            <li className={"mobile__app-advantages-item"}>
                                <p className={"mobile__app-advantages-item-text"}>
                                    Деньги всегда с вами
                                </p>
                            </li>
                        </ul>
                        <Button
                            type={"button"}
                            className={"mobile__app-learn-more-btn"}
                            text={"Узнать больше"}
                        />
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

