import React from "react";
import Slider from "react-slick";
import {slidersSettings} from "../../helpers/helpers";

export default function Reviews() {
    const settings = slidersSettings(1,3);
    return (
        <React.Fragment>
            <section className="review">
                <div className="review__slider">
                    <h2 className={"review__slider-title"}>
                        О нас говорят
                    </h2>
                    <Slider {...settings}>
                        <div className="review__slider-item">
                            <p className={"review__slider-email"}>
                                review user@EMAIL
                            </p>
                            <p className="review__slider-text">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            </p>
                        </div>
                        <div className="review__slider-item">
                            <p className={"review__slider-email"}>
                                review user@EMAIL
                            </p>
                            <p className="review__slider-text">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            </p>
                        </div>
                        <div className="review__slider-item">
                            <p className={"review__slider-email"}>
                                review user@EMAIL
                            </p>
                            <p className="review__slider-text">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            </p>
                        </div>
                        <div className="review__slider-item">
                            <p className={"review__slider-email"}>
                                review user@EMAIL
                            </p>
                            <p className="review__slider-text">
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                irure
                                dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                pariatur.
                                Excepteur sint occaecat cupidatat
                                non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                        </div>
                    </Slider>
                </div>
            </section>
        </React.Fragment>
    )
}

