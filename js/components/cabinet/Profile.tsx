import React from "react";
import cookie from 'react-cookies';
import Input from "../pieces/Input";
import Button from "../pieces/Button";
// import Router from 'next/router';


export default function Profile(props) {
    // useEffect(()=>{
    //     props.getUserLoan();
    // });
    // if (process.browser) {
    let coockData = cookie.load("Auth");

    const name = coockData.lastName + " " + coockData.firstName.slice(0, 1) + ". " + coockData.middleName.slice(0, 1) + ".";
    console.log(coockData);
    // }
    console.log(props);
    return (
        <section className="profile">
            <p className="profile__title">Профиль</p>
            <div className="profile__wrapper">
                <div className="profile__client-info-wrapper">
                    {/*<div className="profile__client-info">*/}
                        <p className="profile__client-photo-wrapper">
                            <img className="profile__client-photo" src="/" alt="ddd"/>
                        </p>
                    {/*</div>*/}
                    <div className="profile__client-wrapper">
                        <p className="profile__client-name">{name}</p>
                        <p className="profile__client-birthdate">{coockData.birthDate}</p>
                    </div>
                </div>
                <div className="editing">
                    <h2 className="editing__title">Выберите раздел данных</h2>
                    <ul className="editing__list">
                        <li className="editing__item">
                            <h3 className="editing__item">
                                Основные данные
                            </h3>
                        </li>
                        <li className="editing__item">
                            <h3 className="editing__item">
                                Основные данные
                            </h3>
                        </li>
                        <li className="editing__item">
                            <h3 className="editing__item">
                                Основные данные
                            </h3>
                        </li>
                        <li className="editing__item">
                            <h3 className="editing__item">
                                Основные данные
                            </h3>
                        </li>
                        <li className="editing__item">
                            <h3 className="editing__item">
                                Основные данные
                            </h3>
                        </li>
                        <li className="editing__item">
                            <h3 className="editing__item">
                                Основные данные
                            </h3>
                        </li>
                        <li className="editing__item">
                            <h3 className="editing__item">
                                Основные данные
                            </h3>
                        </li>
                        <li className="editing__item">
                            <h3 className="editing__item">
                                Основные данные
                            </h3>
                        </li>
                        <li className="editing__item">
                            <h3 className="editing__item">
                                Основные данные
                            </h3>
                        </li>


                    </ul>

                </div>

            </div>
            <Input
                type="checkbox"
                labelText="Подписаться на рассылку"
            />
            <Input
                type="checkbox"
                labelText="Согласие на обработку персональных данных и передачу третьим лмцам"
            />
            <Input
                type="checkbox"
                labelText="Настоящим я подтверждаю, что ознакомлен и согласен"
            />
            <Button
                className="profile"
                text="Сохранить"
            />
        </section>
    )
}

