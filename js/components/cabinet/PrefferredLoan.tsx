import React, {useEffect} from "react";
import {COINS_ICON, RUBLE_ICON} from "../../constants/IconConstants";
import Input from "../pieces/Input";
import Button from "../pieces/Button";
import "../../../scss/components/cabinet/acceptance-offer.scss";
import cookie from 'react-cookies';

export default function PrefferedLoan(props) {
    useEffect(() => {

        props.getOfferInfo();
    }, []);
    console.log(props);
    // const userLoanList = props.cabinet.userLoan.userLoanList !== undefined ? props.cabinet.userLoan.userLoanList : [];


    if (process.browser) {
        let data = cookie.load("Auth");
        console.log(data.extId);
    }
    const acceptanceOffer = () => {
        return (
            <>
                <p className="acceptance-offer__title">
                    Принятие кода офферты
                </p>
                <div className="acceptance-offer__wrapper">
                    <p className="acceptance-offer__img-wrapper">{COINS_ICON}</p>
                    <div className="acceptance-offer__summ-wrapper">
                        <p className="acceptance-offer__summ">
                           5 600 {RUBLE_ICON}
                        </p>
                        <p className="acceptance-offer__summ-text">Одобренная сумма займа</p>
                    </div>

                    <p className="acceptance-offer__phone-text"> Отправленно сообщение с кодом на номер +7 (928) 123 22
                        11</p>
                    <Input
                        type="text"
                        className="acceptance-offer__form"
                        labelText="СМС код"
                    />
                    <p className="acceptance-offer__resent-code">
                        Если сообщение не пришло нажмите кнопку "Выслать код повторно"
                    </p>
                    <Button
                        text="Выслать код повторно"
                        className="acceptance-offer__btn-resent"
                    />
                    <Button
                        text="Отказаться"
                        className="acceptance-offer__btn-refuse"
                    />
                    <Button
                        text="Подтвердить"
                        className="acceptance-offer__btn-submit"
                    />
                    <div className="acceptance-offer__form-checkbox-wrapper">
                        <Input
                            type="checkbox"
                            id="check-offer"
                            className="acceptance-offer__form"
                            labelText={"Я подтверждаю, что ознакомлен и согласен с условиями оферты о заключении" + " договора микрозайма"}
                        />
                        <Input
                            type="checkbox"
                            id="check-insurance"
                            className="acceptance-offer__form"
                            labelText="Преимущества оформления страховки и заключения договора возмездного оказания услуг. Подробнее"
                        />
                    </div>
                </div>
            </>
        )
    };
    return (
        <section className="user-preffered">
            <div className="user-preffered__wrapper">
                {acceptanceOffer()}
            </div>
        </section>
    )
}

