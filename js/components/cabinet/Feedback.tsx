import React from "react";
import "../../../scss/components/cabinet/feedback.scss"
import Input from "../pieces/Input";
import Select from "../pieces/Select";
import Button from "../pieces/Button";
import Textarea from "../pieces/Textarea";

export default function Feedback(props) {

    // const intialState: JSX.Element[] = [];

    // let [input, updateInputsState] = useState(intialState);
    // const addFileInput = () => {
    //     const length = input.length;
    //     console.log(data);
    //     updateInputsState([...input,
    //         <>
    //             <Input
    //                 type="file"
    //                 id={"file" + length}
    //                 className="appeal__form"
    //                 onDrop={e => props.addFile(e, "file" + length)}
    //                 onChange={(e) => props.addFile(e, "file" + length)}
    //                 //     error={errors.photoPassport}
    //                 //     onFocus={() => props.clearError("photoPassport")}
    //                 element={data !== undefined && data["file" + length] !==undefined ?
    //                     <img className="appeal__form-img-loaded" src={data["file" + length]}
    //                          alt="photoPassport"/> : ""}
    //             />
    //             <button
    //                 className="appeal__form-btn-delete"
    //                 data-element={length}
    //                 onClick={e => deleteInput(e)}
    //             >Удалить
    //             </button>
    //         </>
    //     ]);
    //     console.log(input.length)
    // };

    // const deleteInput = (e) => {
    //     let elem = e.currentTarget.getAttribute("data-element");
    //     console.log(elem);
    //     let arr = input;
    //     arr.splice(elem, 1);
    //     updateInputsState(arr);
    // };

    // const renderFileInput = () => {
    //
    //     return input;
    // };

    const data = props.cabinet.feedback.appealFiles;
    const inputs = props.cabinet.feedback.fileInputs;
    // console.log(data);
    console.log(data);
    return (
        <section className="cabinet-feedback">
            <p className="cabinet-feedback__title">Список обращений</p>
            <div className="cabinet-feedback__wrapper">
                <div className="appeal__wrapper">
                    <ul className="appeal__list">
                        <li className="appeal__item">
                            <h3 className="appeal__item-title">
                                Обращение № 435600
                            </h3>
                            <div className="appeal__item-wrapper">
                                <p className="appeal__item-date">
                                    05.04.20199
                                </p>
                                <p className="appeal__item-status">
                                    На рассмотрении
                                </p>
                            </div>
                            <p className=" appeal__item-text">
                                Текстовое наполнение обращения может располагаться не более чем в две строки
                            </p>
                        </li>
                        <li className="appeal__item">
                            <h3 className="appeal__item-title">
                                Обращение № 435600
                            </h3>
                            <div className="appeal__item-wrapper">
                                <p className="appeal__item-date">
                                    05.04.20199
                                </p>
                                <p className="appeal__item-status">
                                    На рассмотрении
                                </p>
                            </div>
                            <p className="appeal__item-text">
                                Текстовое наполнение обращения может располагаться не более чем в две строки
                            </p>
                        </li>
                        <li className="appeal__item">
                            <h3 className="appeal__item-title">
                                Обращение № 435600
                            </h3>
                            <div className="appeal__item-wrapper">
                                <p className="appeal__item-date">
                                    05.04.20199
                                </p>
                                <p className="appeal__item-status">
                                    На рассмотрении
                                </p>
                            </div>
                            <p className="appeal__item-text">
                                Текстовое наполнение обращения может располагаться не более чем в две строки
                            </p>
                        </li>
                    </ul>
                    <Button
                        text="просмотреть все обращения"
                        className="appeal__btn-more"
                    />
                </div>
                <div className="appeal__form">
                    <h3 className="appeal__form-title">
                        Создать новое сообщение
                    </h3>
                    <Select
                        title="Тема обращения"
                        className="appeal__form"
                    />
                    <h3 className="appeal__form-title">
                        Ваше обращение
                    </h3>
                    <Textarea
                        wrap="soft"
                        id="appealText"
                        className="appeal__form"
                        placeholder="Введите сообщение"
                        cols={2}
                        rows={2}
                    />
                    <div className="appeal__form-addfile-wrapper">

                        {/*{renderFileInput()}*/}
                        {/*{input}*/}
                        {inputs !== undefined ? Object.keys(inputs).map(i => {
                            return <>
                                <Input
                                    type="file"
                                    key={i}
                                    id={"file" + inputs.id}
                                    className="appeal__form"
                                    onDrop={e => props.addFile(e, "file" + inputs.id )}
                                    onChange={(e) => props.addFile(e, "file" + inputs.id)}
                                    //     error={errors.photoPassport}
                                    //     onFocus={() => props.clearError("photoPassport")}
                                    element={data !== undefined && [data.inputs] !== undefined ?
                                        <img className="appeal__form-img-loaded" src={data["file" + inputs.id]}
                                             alt="photoPassport"/> : ""}
                                />
                                <button
                                    className="appeal__form-btn-delete"
                                    data-element={inputs.id}
                                    // onClick={e => deleteInput(e)}
                                >Удалить
                                </button>
                            </>

                        }) : ""}
                    </div>
                    <Button
                        text="render"
                        onClick={() => {
                            props.addInputField();
                        }}
                    />
                </div>
            </div>
        </section>
    )
}

