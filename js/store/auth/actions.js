import {
    CLEAR_ERROR,
    SET_AUTH_CODE, SET_AUTH_ERRORS, SET_AUTH_PASSWORD,
    SET_AUTH_PHONE, SET_AUTH_SUCCESS, SET_USER_DATA,
} from "../constants";

import Router from 'next/router';
import cookie from 'react-cookies';
import {clearNumberField} from "../../helpers/helpers";
import { authLogin} from "../../services/Api";

export const setAuthPhone = (userPhone) => {
    return {
        type: SET_AUTH_PHONE,
        payload: userPhone,
    }
};

export const setAuthPassword = (authPassword) => {
    return {
        type: SET_AUTH_PASSWORD,
        payload: authPassword,
    }
};

export const setAuthCode = (authCode) => {
    return {
        type: SET_AUTH_CODE,
        payload: authCode,
    }
};

export const setAuthSucces = (data) => {
    return {
        type: SET_AUTH_SUCCESS,
        payload: data,
    }
};
export const setUserData = (data) => {
    return {
        type: SET_USER_DATA,
        payload: data,
    }
};

export const setAuthErrors= (errors) => {
    return {
        type: SET_AUTH_ERRORS,
        payload: errors,
    }
};
export const clearError = (field) => {
    return {
        type: CLEAR_ERROR,
        payload: field
    }
};

// export const submitAuth = (field) => {
//     return {
//         type: CLEAR_ERROR,
//         payload: field
//     }
// };

export const submitAuth = () => {
    return (dispatch, getState) => {
        const {auth} = getState();
        const authData = {
            phone: clearNumberField(auth.authPhone),
            password: auth.authPassword
        };
        authLogin(authData)
            .then((r) => {
                    if (r.data) {
                        dispatch(setAuthSucces(r.data));
                        cookie.save("Auth",r.data,{ maxAge:"6000"});
                        Router.push('/cabinet');
                    }
                }
            )
            .catch((e) => {
                if(e.response) {
                    if (e.response.status === 404) {
                        dispatch(setAuthErrors({phone: ["Пользователь не найден"]}));
                    } else if (e.response.status === 401) {
                        dispatch(setAuthErrors({phone: ["Неверный логин или пароль"]}));
                    } else {
                        dispatch(setAuthErrors(e.response.data.errors));
                    }
                }
            })
    }
};

