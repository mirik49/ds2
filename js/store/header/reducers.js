import {
    SET_OPEN_MENU
} from "../constants";
//
const defaultState = {
    isOpenMenu: false
};

export const headerReducer = (state = defaultState, action) => {
    // console.log(state);
    switch (action.type) {
        case SET_OPEN_MENU: {
            const body = document.body.classList;
            if(body.contains('overlay')) {

                body.remove('overlay');
            } else {
                body.add('overlay');
            }
            return state = {...state, isOpenMenu: !state.isOpenMenu}
        }

    }
    return state;
};
