import {
 SET_OPEN_MENU
} from "../constants";

export const _setOpenMenu = (isOpenMenu) => {
    return {
        type: SET_OPEN_MENU,
        payload: isOpenMenu,
    }
};

// export const setNewPassword = (newPassword) => {
//     return {
//         type: SET_NEW_PASSWORD,
//         payload: newPassword
//     }
// };
//
// export const setRestoreCode = (restoreCode) => {
//     return {
//         type: SET_RESTORE_CODE,
//         payload: restoreCode
//     }
// };

// export const setRestoreCodeResent= (restoreCodeResent) => {
//     return {
//         type: SET_RESTORE_CODE_RESENT,
//         payload: restoreCodeResent
//     }
// };


