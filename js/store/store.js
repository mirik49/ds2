import {authReducer} from "./auth/reducers";
import {restoreReducer} from "./restore/reducers";
import {headerReducer} from "./header/reducers";
import {calcReducer} from "./calc/reducers";
import {indexReducer} from "./index/reducers";
import {dadataReducer} from "./dadata/reducers";
import {regStep1Reducer} from "./registration/step1/reducers";
import {regStep2Reducer} from "./registration/step2/reducers";
import {regStep3Reducer} from "./registration/step3/reducers";
import {regStep4Reducer} from "./registration/step4/reducers";
import {regStep5Reducer} from "./registration/step5/reducers";
import {regStep6Reducer} from "./registration/step6/reducers";
import regFinalStepReducer from "./registration/final-step/reducers";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";
import {userLoan} from "./cabinet/user-loan/reducers";
import {changePassword} from "./cabinet/change-password/reducers";
import {applyMiddleware, combineReducers, createStore} from 'redux';
import {
    TEST, UPDATE_TOKEN
} from './constants';
import {prefferedLoan} from "./cabinet/prefferedLoan/reducers";
import {feedback} from "./cabinet/feedback/reducers";

export const baseState = {
    fileImg: "",
    date: new Date(),
    goTest: false,
    //cabinet.fiedback
    fileInputs: {},
    appealFiles: {},

    stepOneDataReturn: {},
    stepTwoDataReturn: {},
    stepThreeDataReturn: {},
    stepFourDataReturn: {},
    stepFiveDataReturn: {},
    stepSixDataReturn: {},
    step: 0,
    supportPhone: "8 (800) 700 74 24",
    registrationStatus: "",
    restorePasswordStatus: "",
    newPassword: "",

    restoreCodeResent: "",
    //calc
    defaultCalcData: {},
    defaultDay: 0,
    defaultSum: 0,
    sum: 0,
    sumStep: 0,
    sumMin: 0,
    sumMax: 0,
    dayStep: 0,
    dayMin: 0,
    dayMax: 0,
    promocode: "",
    returnDate: "",
    returnSum: "",
    percent: 0,
    amount: 10000,
    period: 16,

    consentPersonalData: true,
    subscribe: true,
    promoCode: "",
    lastName: "",
    firstName: "",
    middleName: "",
    birthDate: "",
    userGender: 1,
    phone: "",

    passportNumber: "",
    passportSeries: "",
    issuedBy: "",
    issuedDate: "",
    issuedSubdivision: "",
    birthPlace: "",
    snils: "",
    addressReg: {
        region: "",
        postalCode: "",
        area: "",
        city: "",
        settlement: "",
        street: "",
        houseNumber: "",
        blockNumber: "",
        flatNumber: "",
        dateRegistration: "",
    },
    addressAct: {
        region: "",
        postalCode: "",
        area: "",
        city: "",
        settlement: "",
        street: "",
        houseNumber: "",
        blockNumber: "",
        flatNumber: "",
        dateRegistration: "",
    },
    livingByRegAddress: false,

    testData: {
        extId: "1f5db089-ab0b-47b8-b9e8-a1587f53ebaa",
        amount: 10000,
        period: 16,
        promoCode: "",
        lastName: "Певыов",
        firstName: "Иаана",
        middleName: "Сырывзгаеоч",
        birthDate: "1980-01-01",
        userGender: 1,
        phone: "79120000011",
        newsletterSubscription: true,
        series: "0123",
        number: "012345",
        issuedBy: "МВД ПО ЧУВАШСКОЙ РЕСПУБЛИКЕ",
        issuedDate: "2000-01-01",
        issuedSubdivision: "900-003",
        birthPlace: "ГОР. БАРНАУЛ",
        snils: "11969606399",
        livingByRegAddress: true,
        addressReg: {
            region: "Чувашская Республика",
            zip: 295000,
            area: "Бахчисарайский район",
            city: "Казань",
            settlement: "село Подгорное",
            street: "Пушкина",
            houseNumber: 128,
            blockNumber: 128,
            flatNumber: 15,
            dateRegistration: "1990-01-01",
        },
        addressAct: {
            region: "Ростовская обл",
            zip: 295000,
            area: "",
            city: "г Ростов-на-Дону",
            settlement: "",
            street: "ул Портовая",
            houseNumber: "д 191",
            blockNumber: 3,
            flatNumber: 444,
            dateRegistration: "1980-10-10",
        },
        skype: "ClientSkype",
        educationTypeId: "ca4b271d-5db5-11e2-9dcf-001e6712e121",
        familyStatusTypeId: "a2bd8812-7312-11e0-add9-003048dbee1c",
        loanPurposeTypeId: "fa2e9288-84bb-11e7-80eb-001999d8ca9f",
        incomePerMonth: 22000,
        hasOpenedCredits: false,
        amountPayMonthlyCredits: 8000,
        hasOpenedLoans: false,
        amountPayMonthlyLoans: 8000,
        amountTotalOtherObligations: 8000,
        isPensioner: false,
        isUnemployed: false,
        workName: "ООО МКК \"Скорфин\"",
        positionName: "Программист",
        workAddress: "344034, Ростовская обл, город Ростов-На-Дону, улица Портовая, дом 193, ОФИС 302",
        workLengthLastTypeId: "324dba36-8c63-11e2-9d99-001e6712e121",
        phoneWork: "79123456789",
        wasDeclaredAsBankrupt: false,
        guarantorIncome: {
            extId: "МатьОтец",
            FIO: "Петров Иван Сидорович",
            phone: "79123456789"
        },
        guarantorResidence: {
            extId: "МатьОтец",
            FIO: "Петров Иван Сидорович",
            phone: "79123456789"
        },
        agreedWithBki: true,
        agreedWithDdo: true,
        card: {
            cardNumber: "4444444444444448",
            cardMonth: "05",
            cardYear: "22",
            moneyChannelId: "1",
        },
    },
    showRegionHint: false,
    showAreaHint: false,
    showCityHint: false,
    showSettlementHint: false,
    showStreetHint: false,
    showHouseNumberHint: false,
    searchCity: "",
    searchregionArr: {},
    searchAreaArr: {},
    searchCityArr: {},
    searchSettlementArr: {},
    searchStreetArr: {},
    searchHouseArr: {},
    region_fias_id: "",
    area_fias_id: "",
    city_fias_id: "",
    settlement_fias_id: "",
    street_fias_id: "",
    act_region_fias_id: "",
    act_area_fias_id: "",
    act_city_fias_id: "",
    act_settlement_fias_id: "",
    act_street_fias_id: "",
    step2Errors: {
        addressReg: {},
    },
//step3
    guarantorIncome: {},
    guarantorResidence: {},
    maritalStatus: {},
    education: {},
    loanPurpose: {},
    lastWorkLength: {},
    selectedGuarantorIncome: "",
    selectedGuarantorResidence: "",
    selectedEducationId: "",
    selectedMaritalId: "",
    selectedLoanPurposesId: "",
    selectedLastWorkLengthId: "",
    firstGuarantName: "",
    firstGuarantTelephone: "",
    secondGuarantName: "",
    secondGuarantTelephone: "",
    skype: "",
    incomePerMonth: "",

    amountPayMonthlyCredits: "",
    amountPayMonthlyLoans: "",
    hasOpenedCredits: "",//непонятно
    hasOpenedLoans: false,//false
    wasDeclaredAsBankrupt: false,
    amountTotalOtherObligations: "",
    isPensioner: false,
    isUnemployed: true, //непонятно
    workName: "",
    positionName: "",
    workAddress: "",
    phoneWork: "",
    agreedWithBki: false,
    agreedWithDdo: false,
    step3Errors: {},
    //step4
    code: "",
    activateCode: "",
    smsCode: "",
    step4Errors: {},
    step4Message: [],
    //step5
    card: {
        cardNumber: "",
        cardMonth: "",
        cardYear: "",
        moneyChannelId: "",
        selectedChannel: false,
    },
    //step6
    photoWithCodeUrl: "",
    photoWithCode: "",
    photoPassportUrl: "",
    photoPassport: "",
    photoPassportRegUrl: "",
    photoPassportReg: "",
    step6Errors: {},
    //finalStep
    password: "",
    confirmPassword: "",
    finalUserData: {},


    //auth
    authPhone: "",
    authPassword: "",
    authCode: "",
    authSuccessMessage: "",
    authSuccessCode: "",
    showAuthEnterCode: false,
    userData: {},
    authErrors: {},
    step1Errors: {},
    userAuthToken: "",
    //restore
    restorePhone: "",
    restoreSuccess: false,
    testRestoreCode: "",
    restoreMessage: "",
    restoreErrors: {},
    restoreCode: "",
    authData: {},

};

export function initializeStore(initialState = baseState) {
    return createStore(
        mainReducer,
        initialState,
        composeWithDevTools(applyMiddleware(thunk))
    )
}

export const baseReducer = (state = baseState, action) => {
    switch (action.type) {
        case TEST: {
            return {...state, isTest: true}
        }
        case UPDATE_TOKEN: {
            console.log(action.payload);
            return {...state, userAuthToken: action.payload}
        }
    }
    return state;
};

const cabinetReducer = combineReducers({
    userLoan,
    changePassword,
    prefferedLoan,
    feedback
});

const registrationReducer = combineReducers({
    regStep1Reducer,
    regStep2Reducer,
    regStep3Reducer,
    regStep4Reducer,
    regStep5Reducer,
    regStep6Reducer,
    regFinalStepReducer,
});

const mainReducer = combineReducers({
    "base": baseReducer,
    calc: calcReducer,
    dadata: dadataReducer,
    index: indexReducer,
    header: headerReducer,
    restoreReducer: restoreReducer,
    cabinet: cabinetReducer,
    registration: registrationReducer,
    auth: authReducer,

});




