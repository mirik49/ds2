import {
    SET_CAB_CHANGE_PASSWORD_ERRORS,
    SET_CHANGE_NEW_PASSWORD, SET_CHANGE_NEW_PASSWORD_CONFIRM,
    SET_CHANGE_OLD_PASSWORD
} from "../../constants";
import {apiGetOfferInfo, clientChangePassword} from "../../../services/Api";
import cookie from "react-cookies";


export const setChangeOldPassword = (oldPassword) => {
    return {
        type: SET_CHANGE_OLD_PASSWORD,
        payload: oldPassword,
    }
};
export const setChangeNewPassword = (newPassword) => {
    return {
        type: SET_CHANGE_NEW_PASSWORD,
        payload: newPassword,
    }
};

export const setChangeNewPasswordConfirm = (newPasswordConfirm) => {
    return {
        type: SET_CHANGE_NEW_PASSWORD_CONFIRM,
        payload: newPasswordConfirm,
    }
};
export const setCabChangePasswordErrors = (errors) => {
    return {
        type: SET_CAB_CHANGE_PASSWORD_ERRORS,
        payload: errors,
    }
};

export const getOfferInfo = () => {
    return (dispatch) => {
        let data;
        if (process.browser) {
            data = cookie.load("Auth");
        }

        apiGetOfferInfo(data.extId)
            .then((r) => {
                    if (r.data) {
                        // dispatch(setUserLoan(r.data))
                        console.log(r.data);
                    }
                }
            )
            .catch((e) => {
                if (e.response) {
                    dispatch(setCabChangePasswordErrors(e.response.data));
                }
            })
    }
};
