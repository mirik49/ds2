import {uploadPhoto} from "../../../services/Api";
import Input from "../../../components/pieces/Input";
import React from "react";

export const setPhoto = (appealFile, appearUrl, id) => {
    return {
        type: "ADD_APPEAL_FILE",
        payload: {
            file: appealFile,
            url: appearUrl,
            id: id
        }
    }
};

export const setInputs = (data) => {
    return {
        type: "ADD_INPUTS_FILE",
        payload: data
    }
};

export const addInputField = () => {
    return (dispatch, getState) => {
        let {cabinet} = getState();

        let fileInputs = cabinet.fileInputs;
        let data = cabinet.appealFiles;
        console.log(cabinet.fileInputs);
        let length = fileInputs !== undefined ? Object.keys(fileInputs).length : {};
        console.log(length);
        fileInputs = {
            id: 1
        };
        dispatch(setInputs(fileInputs));
        console.log(fileInputs);

    }


};

export const addFile = (e, id) => {
    return (dispatch) => {
        let file;
        if (!e.dataTransfer) {
            //если событие change
            file = e.target.files[0];
        } else {
            //если событие drop
            let dt = e.dataTransfer;
            file = dt.files[0];
        }
        const reader = new FileReader();
        reader.readAsDataURL(file);
        const formData = new FormData();
        formData.append("file", file);
        uploadPhoto(formData).then(function (r) {
            dispatch(setPhoto(reader.result, r.data.path, id));
        });
    }
};

{/*<>*/}
{/*    <Input*/}
{/*        type="file"*/}
{/*        id={"file" + i}*/}
{/*        className="appeal__form"*/}
{/*        onDrop={e => props.addFile(e, "file" + i)}*/}
{/*        onChange={(e) => props.addFile(e, "file" + i)}*/}
{/*        //     error={errors.photoPassport}*/}
{/*        //     onFocus={() => props.clearError("photoPassport")}*/}
{/*        element={data !== undefined && data["file" + i] !== undefined ?*/}
{/*            <img className="appeal__form-img-loaded" src={data["file" + i]}*/}
{/*                 alt="photoPassport"/> : ""}*/}
{/*    />*/}
{/*    <button*/}
{/*        className="appeal__form-btn-delete"*/}
{/*        data-element={i}*/}
{/*        // onClick={e => deleteInput(e)}*/}
{/*    >Удалить*/}
{/*    </button>*/}
{/*</>*/}
