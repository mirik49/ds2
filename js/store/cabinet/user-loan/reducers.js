import {baseState} from "../../store";
import {SET_USER_APPLICATION_DATA, SET_USER_LOAN} from "../../constants";
import {
    getCabinetOptions,
    getUserAplication,
    getUserAplicationData
} from "../../../services/Api";
import {setUserLoan} from "./actions";
import cookie from 'react-cookies';


export const userLoan = (state = baseState, action) => {
    switch (action.type) {
        case SET_USER_LOAN: {
            return state = {...state, userLoanList: action.payload}
        }
        case SET_USER_APPLICATION_DATA: {
            return state = {...state, userApplicationData: action.payload}
        }
    }
    return state;
};


