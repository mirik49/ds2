import {baseState} from "../../store";
import { CLEAR_ERROR_FOUR,
    SET_ACTIVATE_CODE,
    SET_SMS_CODE,
    SET_STEP4_DATA,
    SET_STEP4_ERRORS
} from "../../constants";
export const regStep4Reducer = (state = baseState, action) => {
    switch (action.type) {
        case SET_ACTIVATE_CODE: {
            return {...state, activateCode: action.payload}
        }
        case SET_STEP4_DATA: {
            return {...state, stepFourDataReturn: action.payload, userAuthToken: action.payload.client.token}
        }
        case SET_SMS_CODE: {
            return {...state, smsCode: action.payload}
        }
        case SET_STEP4_ERRORS: {
            return {...state, step4Errors: action.payload.errors,
            step4Message: action.payload.message}
        }
        case CLEAR_ERROR_FOUR: {
            return  {...state, step4Errors: {...state.step4Errors, [action.payload]: ""}, step4Message: []}
        }
    }
    return state;
};
