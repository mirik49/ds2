import { CLEAR_ERROR_FOUR,
    RUN_TEST_STEP3, SET_ACTIVATE_CODE,SET_DATA_STEP3, SET_SMS_CODE, SET_STEP4_DATA, SET_STEP4_ERRORS,
} from "../../constants";
import {registerStepFour, sendActivationCode} from "../../../services/Api";
import Router from "next/router";

export const runTestStep3 = () => {
    return {
        type: RUN_TEST_STEP3,
    }
};

export const setStep3 = (data) => {
    return {
        type: SET_DATA_STEP3,
        payload: data,
    }
};
export const setActivateCode= (code) => {
    return {
        type: SET_ACTIVATE_CODE,
        payload: code,
    }
};

export const setStep4 = (data) => {
    return {
        type: SET_STEP4_DATA,
        payload: data
    }
};

export const setSmsCode = (code) => {
    return {
        type: SET_SMS_CODE,
        payload: code
    }
};
export const setStep4Errors = (errors) => {
    return {
        type: SET_STEP4_ERRORS,
        payload: errors
    }
};
export const clearErrorFour = (field) => {
    console.log("clear");
    return {
        type: CLEAR_ERROR_FOUR,
        payload: field
    }
};

export const submitStepFour = (history) => {
    return (dispatch, getState) => {
        const {registration} = getState();
        const data = registration.regStep4Reducer;
        const extId = registration.regStep3Reducer.stepThreeDataReturn.extId;
        const registerData = {
            extId: extId,
            activateCode: data.activateCode,
        };
        registerStepFour(registerData)
            .then(function (r) {
                if (r.data) {
                    dispatch(setStep4(r.data));
                    Router.push("/five");
                }
            })
            .catch((e) => {
                dispatch(setStep4Errors(e.response.data));
            })
    }
};

export const sentActivateCode = () => {
    return (dispatch, getState) => {
        const {registration} = getState();

        const extId = registration.regStep3Reducer.stepThreeDataReturn.extId;
        console.log(extId);

        const data = {
            extId: extId
        };
        sendActivationCode(data)
            .then(function (r) {
                if (r.data) {
                    dispatch(setSmsCode(r.data.smsCode));
                }
            })
    }
};


