import {baseState} from "../../store";
import {
    RUN_TEST_STEP5,
    SET_CARD_MONTH,
    SET_CARD_NUMBER,
    SET_CARD_YEAR, SET_DATA_STEP5, SET_MONEY_CHANNEL_ID
} from "../../constants";

export const regStep5Reducer = (state = baseState, action) => {
    switch (action.type) {
        case RUN_TEST_STEP5: {
            return state = {...state, goTest: true}
        }
        case SET_CARD_NUMBER: {
            return {...state, card: {...state.card, cardNumber: action.payload}}
        }
        case SET_CARD_MONTH: {
            return {...state, card: {...state.card, cardMonth: action.payload}}
        }
        case SET_CARD_YEAR: {
            return {...state, card: {...state.card, cardYear: action.payload}}
        }
        case SET_MONEY_CHANNEL_ID: {
            return {
                ...state,
                card: {...state.card, moneyChannelId: action.payload},
                selectedChannel: !state.selectedChannel
            }
        }
        case SET_DATA_STEP5: {
            return {...state, stepFiveDataReturn: action.payload}
        }
    }
    return state;
};

