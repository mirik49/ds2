import {
    CLEAR_ERROR,REDIRECT,
    RUN_TEST,
    SET_BIRTHDAY_DATE,
    SET_CONSENT_PERSONAL_DATA,
    SET_GENDER,
    SET_NAME,
    SET_PATRONYMIC,
    SET_PHONE,
    SET_SECOND_NAME,
    SET_STEP, SET_STEP1_ERRORS,
    SET_SUBSCRIBE,
    TEST
} from "../../constants";
import Router from "next/router";
import {clearNumberField} from "../../../helpers/helpers";
import {registerStepOne} from "../../../services/Api";

export const runTest = () => {
    return {
        type: RUN_TEST,
        payload: true
    }
};

export const test = (data) => {
    return {
        type: TEST,
        payload: data
    }
};

export const setStep = (data) => {

    return {
        type: SET_STEP,
        payload: data
    }
};

export const setPhone = (userPhone) => {
    return {
        type: SET_PHONE,
        payload: userPhone,
    }
};

export const setName = (userName) => {
    return {
        type: SET_NAME,
        payload: userName,
    }
};

export const setSecondName = (userSecondName) => {
    return {
        type: SET_SECOND_NAME,
        payload: userSecondName,
    }
};

export const setPatronymic = (userPatronymic) => {
    return {
        type: SET_PATRONYMIC,
        payload: userPatronymic,
    }
};

export const setBirthDate = (userBirthDate) => {
    return {
        type: SET_BIRTHDAY_DATE,
        payload: userBirthDate,
    }
};

export const setGender = (userGender) => {
    return {
        type: SET_GENDER,
        payload: userGender,
    }
};

export const setSubscribe = (subscribe) => {
    return {
        type: SET_SUBSCRIBE,
        payload: subscribe,
    }
};

export const setConsentPersonalData = (consentPersonalData) => {
    return {
        type: SET_CONSENT_PERSONAL_DATA,
        payload: consentPersonalData,
    }
};

export const redirect = (url) => {
    console.log("redirect action");
    return {
        type: REDIRECT,
        payload: url,
    }
};

export const setStep1Errors = (errors) => {
    return {
        type: SET_STEP1_ERRORS,
        payload: errors,
    }
};
export const clearError = (field) => {
    return {
        type: CLEAR_ERROR,
        payload: field
    }
};

export const submitStepOne = () => {
    return (dispatch, getState) => {
        const {registration} = getState();
        const data = registration.regStep1Reducer;
        const testData = data.testData;
        const goTest = data.goTest;
        const registerData = {
            projectId: "c6e666d8-99e1-4544-8327-d9b8b0d9c5f1",//c6e666d8-99e1-4544-8327-d9b8b0d9c5f1
            sourceId: "",
            amount: 10000,
            period: 16,
            promoCode: goTest ? testData.promocode : data.promoCode,
            lastName: goTest ? testData.lastName : data.lastName,
            firstName: goTest ? testData.firstName : data.firstName,
            middleName: goTest ? testData.middleName : data.middleName,
            birthDate: goTest ? testData.birthDate : data.birthDate,
            gender: goTest ? testData.userGender : data.userGender,
            phone: goTest ? testData.phone : clearNumberField(data.phone),
            newsletterSubscription: false,
        };
        registerStepOne(registerData)
            .then(function (r) {
                if (r.data) {
                    dispatch(setStep(r.data));
                    Router.push("/two");
                }
            })
            .catch((e) => {
                console.log(e);
                if (e.response) {
                    dispatch(setStep1Errors(e.response.data.errors));
                }
            })
    }
};
