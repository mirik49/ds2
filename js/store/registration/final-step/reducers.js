import {baseState} from "../../store";
import {
    RUN_TEST_STEP5, SET_CONFIRM_PASSWORD, SET_FINAL_USER_DATA, SET_PASSWORD,
} from "../../constants";

export default function regFinalStepReducer (state = baseState, action)  {
    switch (action.type) {
        case RUN_TEST_STEP5: {
            return state = {...state, goTest: true}
        }
        case SET_PASSWORD: {
            return {...state, password: action.payload}
        }
        case SET_CONFIRM_PASSWORD: {
            return {...state, confirmPassword: action.payload}
        }
        case SET_FINAL_USER_DATA: {
            return {...state, finalUserData: action.payload}
        }
    }
    return state;
};
