import {
    CLEAR_ERROR,
    RUN_TEST_STEP2,
    SET_ACT_AREA,
    SET_ACT_BLOCK_NUMBER,
    SET_ACT_CITY,
    SET_ACT_DATE_REGISTRATION,
    SET_ACT_FLAT_NUMBER,
    SET_ACT_FLAT_POSTAL_CODE,
    SET_ACT_HOUSE_NUMBER,
    SET_ACT_REGION,
    SET_ACT_SETTLEMENT,
    SET_ACT_STREET,
    SET_AREA,
    SET_BIRTH_PLACE,
    SET_BLOCK_NUMBER,
    SET_CITY,
    SET_DATE_REGISTRATION,
    SET_FLAT_NUMBER,
    SET_FLAT_POSTAL_CODE,
    SET_HOUSE_NUMBER,
    SET_ISSUED_BY,
    SET_ISSUED_DATE,
    SET_ISSUED_SUBDIVISION,
    SET_LIVING_BY_REG_ADDRES,
    SET_PASSPORT_NUMBER,
    SET_REGION,
    SET_SELECTED_ACT_AREA,
    SET_SELECTED_ACT_CITY, SET_SELECTED_ACT_HOUSE_NUMBER,
    SET_SELECTED_ACT_REGION,
    SET_SELECTED_ACT_SETTLEMENT,
    SET_SELECTED_ACT_STREET,
    SET_SELECTED_AREA,
    SET_SELECTED_CITY,
    SET_SELECTED_HOUSE_NUMBER,
    SET_SELECTED_REGION,
    SET_SELECTED_SETTLEMENT,
    SET_SELECTED_STREET,
    SET_SETTLEMENT,
    SET_SNILS,
    SET_STEP2,
    SET_STEP2_ERRORS,
    SET_STREET,
} from "../../constants";
import {registerStepTwo} from "../../../services/Api";
import Router from "next/router";

export const setStep2 = (data) => {

    return {
        type: SET_STEP2,
        payload: data
    }
};

export const runTest = () => {
    return {
        type: RUN_TEST_STEP2,
        payload: true
    }
};

export const setPasportNumber = (passportNumber) => {
    return {
        type: SET_PASSPORT_NUMBER,
        payload: {
            series:passportNumber.slice(0,4),
            number:passportNumber.slice(4,11)
        },
    }
};
export const setIssuedBy = (issuedBy) => {
    return {
        type: SET_ISSUED_BY,
        payload: issuedBy,
    }
};
export const setIssuedDate = (issuedDate) => {
    return {
        type: SET_ISSUED_DATE,
        payload: issuedDate,
    }
};
export const setIssuedSubdivision = (issuedSubdivision) => {
    return {
        type: SET_ISSUED_SUBDIVISION,
        payload: issuedSubdivision,
    }
};
export const setBirthPlace = (birthPlace) => {
    return {
        type: SET_BIRTH_PLACE,
        payload: birthPlace,
    }
};
export const setSnils = (snils) => {
    return {
        type: SET_SNILS,
        payload: snils,
    }
};

//registration
export const setLivingByRegAddress = (livingByRegAddress) => {
    return {
        type: SET_LIVING_BY_REG_ADDRES,
        payload: livingByRegAddress,
    }
};
export const setRegion = (region) => {
    return {
        type: SET_REGION,
        payload: region,
    }
};

export const setArea = (area) => {
    return {
        type: SET_AREA,
        payload: area,
    }
};
export const setCity = (city) => {
    return {
        type: SET_CITY,
        payload: city,
    }
};
export const setSettlement = (settlement) => {
    return {
        type: SET_SETTLEMENT,
        payload: settlement,
    }
};
export const setStreet = (street) => {
    return {
        type: SET_STREET,
        payload: street,
    }
};
export const setHouseNumber = (houseNumber) => {
    return {
        type: SET_HOUSE_NUMBER,
        payload: houseNumber,
    }
};
export const setBlockNumber = (blockNumber) => {
    return {
        type: SET_BLOCK_NUMBER,
        payload: blockNumber,
    }
};
export const setFlatNumber = (flatNumber) => {
    return {
        type: SET_FLAT_NUMBER,
        payload: flatNumber,
    }
};

export const setPostalCode = (postalCode) => {
    return {
        type: SET_FLAT_POSTAL_CODE,
        payload: postalCode,
    }
};
export const setDateRegistration = (dateRegistration) => {
    return {
        type: SET_DATE_REGISTRATION,
        payload: dateRegistration,
    }
};

export const setActRegion = (actRegion) => {
    return {
        type: SET_ACT_REGION,
        payload: actRegion,
    }
};

export const setActArea = (actArea) => {
    return {
        type: SET_ACT_AREA,
        payload: actArea,
    }
};
export const setActCity = (actCity) => {
    return {
        type: SET_ACT_CITY,
        payload: actCity,
    }
};
export const setActSettlement = (actSettlement) => {
    return {
        type: SET_ACT_SETTLEMENT,
        payload: actSettlement,
    }
};
export const setActStreet = (actStreet) => {
    return {
        type: SET_ACT_STREET,
        payload: actStreet,
    }
};
export const setActHouseNumber = (actHouseNumber) => {
    return {
        type: SET_ACT_HOUSE_NUMBER,
        payload: actHouseNumber,
    }
};
export const setActBlockNumber = (actBlockNumber) => {
    return {
        type: SET_ACT_BLOCK_NUMBER,
        payload: actBlockNumber,
    }
};
export const setActFlatNumber = (actFlatNumber) => {
    return {
        type: SET_ACT_FLAT_NUMBER,
        payload: actFlatNumber,
    }
};

export const setActPostalCode = (actPostalCode) => {
    return {
        type: SET_ACT_FLAT_POSTAL_CODE,
        payload: actPostalCode,
    }
};
export const setActDateRegistration = (actDateRegistration) => {
    return {
        type: SET_ACT_DATE_REGISTRATION,
        payload: actDateRegistration,
    }
};

export const setSelectedRegion = (selectedActRegion) => {
    return {
        type: SET_SELECTED_REGION,
        payload: {
            text: selectedActRegion.textContent,
            data: selectedActRegion.getAttribute("region_fias_id")
        }
    }
};
export const setSelectedArea = (selectedActArea) => {
    return {
        type: SET_SELECTED_AREA,
        payload: {
            text:selectedActArea.textContent,
            data: selectedActArea.getAttribute("area_fias_id")
        }
    }
};
export const setSelectedCity = (selectedActCity) => {
    return {
        type: SET_SELECTED_CITY,
        payload: {
            text: selectedActCity.textContent,
            data: selectedActCity.getAttribute("city_fias_id"),
            postalCode: selectedActCity.getAttribute("postal_code"),
        }
    }
};
export const setSelectedSettlement = (selectedActSettlement) => {
    return {
        type: SET_SELECTED_SETTLEMENT,
        payload: {
            text: selectedActSettlement.textContent,
            data: selectedActSettlement.getAttribute("settlement_fias_id")
        }
    }
};
export const setSelectedStreet = (selectedActStreet) => {
    return {
        type: SET_SELECTED_STREET,
        payload: {
            text: selectedActStreet.textContent,
            data: selectedActStreet.getAttribute("street_fias_id")
        }
    }
};
export const setSelectedHouseNumber = (selectedActHouseNumber) => {
    return {
        type: SET_SELECTED_HOUSE_NUMBER,
        payload: {
            text: selectedActHouseNumber.textContent
        }
    }
};

export const setSelectedActRegion = (selectedRegion) => {
    console.log("click action act");
    return {
        type: SET_SELECTED_ACT_REGION,
        payload: {
            text: selectedRegion.textContent,
            data: selectedRegion.getAttribute("region_fias_id")
        }
    }
};
export const setSelectedActArea = (selectedArea) => {
    return {
        type: SET_SELECTED_ACT_AREA,
        payload: {
            text:selectedArea.textContent,
            data: selectedArea.getAttribute("area_fias_id")
        }
    }
};
export const setSelectedActCity = (selectedCity) => {
    return {
        type: SET_SELECTED_ACT_CITY,
        payload: {
            text: selectedCity.textContent,
            data: selectedCity.getAttribute("city_fias_id"),
            postalCode: selectedCity.getAttribute("postal_code"),
        }
    }
};
export const setSelectedActSettlement = (selectedSettlement) => {
    return {
        type: SET_SELECTED_ACT_SETTLEMENT,
        payload: {
            text: selectedSettlement.textContent,
            data: selectedSettlement.getAttribute("settlement_fias_id")
        }
    }
};
export const setSelectedActStreet = (selectedStreet) => {
    return {
        type: SET_SELECTED_ACT_STREET,
        payload: {
            text: selectedStreet.textContent,
            data: selectedStreet.getAttribute("street_fias_id")
        }
    }
};
export const setSelectedActHouseNumber = (selectedHouseNumber) => {
    return {
        type: SET_SELECTED_ACT_HOUSE_NUMBER,
        payload: {
            text: selectedHouseNumber.textContent
        }
    }
};

export const setStep2Errors = (errors) => {
    return {
        type: SET_STEP2_ERRORS,
        payload: errors,
    }
};

export const clearError = (field) => {
    return {
        type: CLEAR_ERROR,
        payload: field
    }
};

export const submitStepTwo = () => {
    return (dispatch, getState) => {
        const {registration} = getState();
        const data = registration.regStep2Reducer;
        const livingByRegAddress = registration.regStep2Reducer.livingByRegAddress;
        const extId = registration.regStep1Reducer.stepOneDataReturn.extId;
        const testData = data.testData;
        const goTest = data.goTest;
        let registerData;
        const adressReg = {
            region: goTest ? testData.addressReg.region : data.addressReg.region,
            zip: goTest ? testData.addressReg.zip : data.addressReg.postalCode,
            area: goTest ? testData.addressReg.area : data.addressReg.area,
            city: goTest ? testData.addressReg.city : data.addressReg.city,
            street: goTest ? testData.addressReg.street : data.addressReg.street,
            house: goTest ? testData.addressReg.houseNumber : data.addressReg.houseNumber,
            block: goTest ? testData.addressReg.blockNumber : data.addressReg.blockNumber,
            flat: goTest ? testData.addressReg.flatNumber : data.addressReg.flatNumber,
            regDate: goTest ? testData.addressReg.dateRegistration : data.addressReg.dateRegistration,
        };
        if(livingByRegAddress) {
            registerData = {
                extId: extId,//data.registration.stepOneDataReturn.extId,
                series: goTest ? testData.series : data.passportSeries,
                number: goTest ? testData.number : data.passportNumber,
                issuedDate: goTest ? testData.issuedDate : data.issuedDate,
                issuedBy: goTest ? testData.issuedBy : data.issuedBy,
                issuedSubdivision: goTest ? testData.issuedSubdivision : data.issuedSubdivision,
                birthPlace: goTest ? testData.birthPlace : data.birthPlace,
                snils: goTest ? testData.snils : data.snils,
                livingByRegAddress: goTest ? testData.livingByRegAddress : data.livingByRegAddress,
                addressReg: adressReg,
            };
        } else {
            registerData = {
                extId: extId,//idata.registration.stepOneDataReturn.extId,
                series: goTest ? testData.series : data.passportSeries,
                number: goTest ? testData.number : data.passportNumber,
                issuedDate: goTest ? testData.issuedDate : data.issuedDate,
                issuedBy: goTest ? testData.issuedBy : data.issuedBy,
                issuedSubdivision: goTest ? testData.issuedSubdivision : data.issuedSubdivision,
                birthPlace: goTest ? testData.birthPlace : data.birthPlace,
                snils: goTest ? testData.snils : data.snils,
                livingByRegAddress: goTest ? testData.livingByRegAddress : data.livingByRegAddress,
                addressReg: adressReg,
                addressAct: {
                    region: goTest ? testData.addressAct.region : data.addressAct.region,
                    zip: goTest ? testData.addressAct.zip : data.addressAct.postalCode,
                    area: goTest ? testData.addressAct.area : data.addressAct.area,
                    city: goTest ? testData.addressAct.city : data.addressAct.city,
                    street: goTest ? testData.addressAct.street : data.addressAct.street,
                    house: goTest ? testData.addressAct.houseNumber : data.addressAct.houseNumber,
                    block: goTest ? testData.addressAct.blockNumber : data.addressAct.blockNumber,
                    flat: goTest ? testData.addressAct.flatNumber : data.addressAct.flatNumber,
                    regDate: goTest ? testData.addressAct.dateRegistration : data.addressAct.dateRegistration,
                }
            };
        }
        registerStepTwo(registerData)
            .then(function (r) {
                if (r.data) {
                    dispatch(setStep2(r.data));
                    Router.push("/three");
                }
            })
            .catch((e) => {
                if (e.response) {
                    dispatch(setStep2Errors(e.response.data.errors));
                }
            })
    }
};
