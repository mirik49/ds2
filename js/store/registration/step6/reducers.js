import {baseState} from "../../store";
import {
    CLEAR_ERROR,
    RUN_TEST_STEP5,
    SET_DATA_STEP5, SET_DATA_STEP6,
    SET_PHOTO_PASSPORT,
    SET_PHOTO_PASSPORT_REG,
    SET_PHOTO_PASSPORT_REG_URL,
    SET_PHOTO_PASSPORT_URL,
    SET_PHOTO_WITH_CODE,
    SET_PHOTO_WITH_CODE_URL, SET_STEP6_ERRORS,
} from "../../constants";

export const regStep6Reducer = (state = baseState, action) => {
    switch (action.type) {
        case RUN_TEST_STEP5: {
            return state = {...state, goTest: true}
        }
        case SET_PHOTO_WITH_CODE_URL: {
            return {...state, photoWithCodeUrl: action.payload}
        }
        case SET_DATA_STEP5: {
            return {...state, stepFiveDataReturn: action.payload}
        }
        case SET_PHOTO_WITH_CODE: {
            return {...state, photoWithCode: action.payload}
        }
        case SET_PHOTO_PASSPORT_URL: {
            return {...state, photoPassportUrl: action.payload}
        }
        case SET_PHOTO_PASSPORT: {
            return {...state, photoPassport: action.payload}
        }
        case SET_PHOTO_PASSPORT_REG_URL: {
            return {...state, photoPassportRegUrl: action.payload}
        }
        case SET_PHOTO_PASSPORT_REG: {
            return {...state, photoPassportReg: action.payload}
        }
        case SET_DATA_STEP6: {
            return {...state, stepSixDataReturn: action.payload}
        }
        case SET_STEP6_ERRORS: {
            return {...state, step6Errors: action.payload.errors}
        }
        case CLEAR_ERROR: {
            return {...state, step6Errors: {...state.step6Errors, [action.payload]: ""}}
        }
    }
    return state;
};
