import {
    CLEAR_ERROR,
    SET_NEW_PASSWORD, SET_RESTORE_CODE, SET_RESTORE_CODE_RESENT, SET_RESTORE_ERRORS,
    SET_RESTORE_PHONE, SET_RESTORE_SUCCES
} from "../constants";
import {authGetPassword, authLogin} from "../../services/Api";
import {setRestoreErrors, setRestoreSucces} from "./actions";
import Router from 'next/router';
import cookie from 'react-cookies';
import {clearNumberField} from "../../helpers/helpers";
export const restoreReducer = (state = {}, action) => {
    switch (action.type) {
        case SET_RESTORE_PHONE: {
            return state = {...state, restorePhone: action.payload}
        }
        case SET_NEW_PASSWORD: {
            return state = {...state, newPassword: action.payload}
        }
        case SET_RESTORE_CODE: {
            return state = {...state, restoreCode: action.payload}
        }

        case SET_RESTORE_CODE_RESENT: {
            return state = {...state, restoreCodeResent: action.payload}
        }
        case CLEAR_ERROR: {
            return {...state, restoreErrors: {...state.restoreErrors, [action.payload]: ""}}
        }
        case SET_RESTORE_SUCCES: {
            return state = {
                ...state, restoreSuccess: true,
                isAuth: true,
                testRestoreCode: action.payload.code,
                restoreMessage: action.payload.message,
                userAuthToken: action.payload.token,
                authData: action.payload,
            }
        }
        case SET_RESTORE_ERRORS: {
            return {...state, restoreErrors: action.payload}
        }
    }
    return state;
};
export const restorePassword = () => {
    return (dispatch, getState) => {
        const {restoreReducer} = getState();
        const authData = {
            phone: clearNumberField(restoreReducer.restorePhone),
        };
        authGetPassword(authData)
            .then((r) => {
                    if (r.data) {
                        dispatch(setRestoreSucces(r.data));
                        console.log(r.data);
                    }
                }
            )
            .catch((e) => {
                if (e.response) {
                    if (e.response.status === 404) {
                        dispatch(setRestoreErrors({phone: ["Пользователь не найден"]}));
                    } else {
                        dispatch(setRestoreErrors(e.response.data.errors));
                    }
                }
            })
    }
};
export const restoreAuth = (history) => {
    return (dispatch, getState) => {
        const {restoreReducer} = getState();
        const phone = restoreReducer.restorePhone.replace(/[^-0-9]/gim, '');
        const authData = {
            phone: phone,
            password: restoreReducer.restoreCode
        };
        authLogin(authData)
            .then((r) => {
                    if (r.data) {

                        cookie.save("Auth",r.data,{ maxAge:"6000"});
                        // console.log(r.data.token);
                        dispatch(setRestoreSucces(r.data));
                        Router.push('/cabinet');
                    }
                }
            )
            .catch((e) => {
                if (e.response) {
                    if (e.response.status === 404) {
                        dispatch(setRestoreErrors({phone: ["Пользователь не найден"]}));
                    } else {
                        dispatch(setRestoreErrors(e.response.data.errors));
                    }
                }
            })
    }
};
