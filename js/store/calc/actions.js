import {SET_CALC_VALUE, SET_DEFAULT_DATA, SET_PROMOCODE
} from "../constants";
import {getCalcSetting} from "../../services/Api";
import moment from "moment";

export const setPromocode= (promocode) => {
    return {
        type: SET_PROMOCODE,
        payload: promocode
    }
};

export const setDefaultData = (defaultData) => {
    return {
        type: SET_DEFAULT_DATA,
        payload: defaultData,
    }
};

export const setCalcValue = (returnDate,returnSum,defaultSum) => {
    return {
        type: SET_CALC_VALUE,
        payload: {
            returnDate: returnDate,
            returnSum: returnSum,
            defaultSum: defaultSum
        },
    }
};

export const _getDefaultData = () => {
    return (dispatch) => {
        getCalcSetting().then(function (r) {
            dispatch(setCalcSum(r.data));
        });
    }
};

export const setCalcSum = (data) => {
    return (dispatch, getState) => {
        const {calc} = getState();
        let products;
        let defaultData;
        let defaultSum;
        if (typeof data === "object") {
            dispatch(setDefaultData(data));
            products = data.products[0];
            defaultSum = data.defaultSum;
            defaultData = data;
        } else {
            products = calc.defaultCalcData.products[0];
            defaultData = calc.defaultCalcData;
            defaultSum = data
        }
        let returnSum = defaultSum * (defaultData.defaultPeriod - 1) * products.percent;
        let date = moment().add(defaultData.defaultPeriod - 1, 'days');
        let returnDate = moment(date).format('DD.MM.YYYY');
        dispatch(setCalcValue(returnDate, returnSum, defaultSum));
    }
};
