import {
    SET_CALC_VALUE, SET_DEFAULT_DATA,
    SET_PROMOCODE, SET_SUM
} from "../constants";
import {baseState} from "../store";

export const calcReducer = (state = baseState, action) => {
    switch (action.type) {
        case SET_SUM: {
            return state = {...state, sum: action.payload}
        }

        case SET_PROMOCODE: {
            return state = {...state, promocode: action.payload}
        }
        case SET_CALC_VALUE: {
            return state = {
                ...state,
                returnDate: action.payload.returnDate,
                returnSum: action.payload.returnSum,
                sum: action.payload.defaultSum
            }
        }
        case SET_DEFAULT_DATA: {
            return state = {
                ...state, defaultCalcData: action.payload,
                defaultDay: action.payload.products[0].day,
                defaultSum: action.payload.products[0].sum,
            }
        }
    }
    return state;
};
