import {dadataGetCity} from "../../services/Api";
import {baseState} from "../store";
import {
    DADATA_SEARCH_AREA,
    DADATA_SEARCH_CITY, DADATA_SEARCH_HOUSE_NUMBER,
    DADATA_SEARCH_REGION,
    DADATA_SEARCH_SETTLEMENT,
    DADATA_SEARCH_STREET,
} from "../constants";
export const dadataReducer = (state = baseState, action) => {
    switch (action.type) {
        case DADATA_SEARCH_REGION: {
            return {...state, searchRegionArr: action.payload.regionList,
                region_fias_id: action.payload.regionList
            }
        }
        case DADATA_SEARCH_AREA: {
            return {
                ...state,
                searchAreaArr: action.payload.areaList,
                area_fias_id: action.payload.areaId
            }
        }
        case DADATA_SEARCH_CITY: {
            console.log(action.payload);
            return {
                ...state,
                searchCityArr: action.payload,
                // city_fias_id: action.payload.cityId
            }
        }
        case DADATA_SEARCH_SETTLEMENT: {
            return {
                ...state,
                searchSettlementArr: action.payload,
                settlement_fias_id: action.payload.suggestions[0].data.settlement_fias_id
            }
        }

        case DADATA_SEARCH_STREET: {
            console.log(action.payload);
            return {
                ...state,
                searchStreetArr: action.payload,
                street_fias_id: action.payload.suggestions[0].data.street_fias_id ? action.payload.suggestions[0].data.street_fias_id : ""
            }
        }
        case DADATA_SEARCH_HOUSE_NUMBER: {
            return {...state, searchHouseArr: action.payload}
        }
    }
    return state;
};


