import Calc from "../components/index/Calc";
import {connect} from "react-redux";
import {_getDefaultData, setCalcSum,} from "../store/calc/actions";
import {setPromocode} from "../store/calc/actions";
// import {setDay, setPromocode, setReturnDate, setReturnSum, setSum} from "../store/calc/actions";
// import moment from "moment";

const mapDispatchToProps = {
    // setSum,
    // setDay,
    setPromocode,
    // setReturnDate,
    // setReturnSum
    _getDefaultData,
    setCalcSum
};

export default connect(state=>state, mapDispatchToProps)(Calc);
