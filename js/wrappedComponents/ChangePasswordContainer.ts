import {connect} from 'react-redux';
import ChangePassword from "../components/cabinet/ChangePassword";
import {
    setChangeNewPassword, setChangeNewPasswordConfirm,
    setChangeOldPassword,
    submitClientChangePassword
} from "../store/cabinet/change-password/actions";

const mapDispatchToProps = {
    setChangeOldPassword,
    setChangeNewPassword,
    setChangeNewPasswordConfirm,
    submitClientChangePassword
};
export default connect(state=>state,mapDispatchToProps)(ChangePassword);
