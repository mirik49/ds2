import {connect} from 'react-redux';
import PrefferredLoan from "../components/cabinet/PrefferredLoan";
import {getOfferInfo} from "../store/cabinet/prefferedLoan/actions"

const mapDispatchToProps = {
    getOfferInfo
};
export default connect(state=>state,mapDispatchToProps)(PrefferredLoan);
