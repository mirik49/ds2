import {connect} from 'react-redux';
import UserLoan from "../components/cabinet/UserLoan";
import {getLoanData} from "../store/cabinet/user-loan/actions";
import {getClientAgreements} from "../store/cabinet/user-loan/actions";

const mapDispatchToProps = {

    getLoanData,
    getClientAgreements
};
export default connect(state=>state,mapDispatchToProps)(UserLoan);
