import axios from "axios";

const CALC_SETTING = "calculate";
const REGISTER_STEP_ONE = "register/one";
const REGISTER_STEP_TWO = "register/two";
const REGISTER_STEP_THREE = "register/three";
const REGISTER_STEP_FOUR = "register/four";
const SENT_ACTIVATION_CODE = "activate/send-code";
const REGISTER_STEP_FIVE = "register/five";
const REGISTER_STEP_SIX = "register/six";
const SET_OR_CHANGE_PASSWORD = "client/change-password";
const AUTH_GET_PASSWORD = "auth/get-password";
const AUTH_LOGIN = "auth/login";
const REGISTER_STEP_SIX_UPLOAD_PHOTO = "files/upload-photo";
const GET_GUARANTOR_INCOME = "directory/guarantor-income";
const GET_GUARANTOR_RESIDENCE = "guarantor-residence";
const GET_MARITAL_STATUS = "directory/marital-status";
const GET_EDUCATION = "directory/education";
const GET_LOAN_PURPOSE = "directory/loan-purpose";
const GET_LAST_WORK_LENGTH = "directory/last-work-length";
const GET_USER_APLICATION_DATA = "client/application";
const CLIENT_CHANGE_PASSWORD = "client/change-password";
const GET_CLIENT_AGREEMENTS = "client/agreements";
const GET_CLIENT_APLICATION = "client/applications";
// const GET_CLIENT_CARD = "client/cards";
// const GET_OFFER_INFO = "/application/{appId}/offer/info";
export function getCalcSetting() {
    return api(CALC_SETTING, 'get',{}, );
}

export function registerStepOne(data) {
    return api(REGISTER_STEP_ONE, 'post', data);
}

export function registerStepTwo(data) {
    return api(REGISTER_STEP_TWO, 'post', data);
}

export function registerStepThree(data) {
    return api(REGISTER_STEP_THREE, 'post', data);
}

export function registerStepFour(data) {
    return api(REGISTER_STEP_FOUR, 'post', data);
}

export function registerStepFive(data) {
    return api(REGISTER_STEP_FIVE, 'post', data);
}

export function registerStepSix(data) {
    return api(REGISTER_STEP_SIX, 'post', data);
}

export function setOrChangePassword(data) {
    return api(SET_OR_CHANGE_PASSWORD, 'post', data);
}

export function sendActivationCode(data) {
    return api(SENT_ACTIVATION_CODE, 'post', data);
}

export function authLogin(data) {
    return api(AUTH_LOGIN, 'post',data);
}

export function authGetPassword(data) {
    return api(AUTH_GET_PASSWORD, 'post', data);
}

export function uploadPhoto(data) {
    return api(REGISTER_STEP_SIX_UPLOAD_PHOTO, 'post', data);
}

export function getOptions() {
    return api(GET_MARITAL_STATUS, 'options', {});
}

export function getMaritalStatus() {
    return api(GET_MARITAL_STATUS, 'get');
}

export function getEducation() {
    return api(GET_EDUCATION, 'get');
}

export function getLoanPurpose() {
    return api(GET_LOAN_PURPOSE, 'get');
}

export function getLastWorkLength() {
    return api(GET_LAST_WORK_LENGTH, 'get',);
}

export function getGuarantorIncome() {
    return api(GET_GUARANTOR_INCOME, 'get');
}

export function getGuarantorResidence() {
    return api(GET_GUARANTOR_RESIDENCE, 'get');
}

//cabinet
export function getCabinetOptions() {
    return api("", 'options');
}

export function clientChangePassword(data) {
    return api(CLIENT_CHANGE_PASSWORD, 'post',data);
}



export function getUserAplicationData(data) {
    return api(GET_USER_APLICATION_DATA, 'post', data);
}
export function apiGetOfferInfo(extId) {

    const GET_OFFER_INFO = "application/{appId}/offer/info";
    let str = GET_OFFER_INFO.replace(/{appId}/gi,extId );
    return api(str, 'get');
}

export function apiGetClientAgreements(data) {
    return api(GET_CLIENT_AGREEMENTS, 'post', data);
}
export function apiGetClientAplication(data) {
    return api(GET_CLIENT_APLICATION, 'post', data);
}

export function dadataGetData(data) {
    //мой код 0d0d9ff7ec3ce2f8abb613d78394dd8dd83f6446
    //код сайта 023f8f0ffea536221ced62d4ced21dfd7c2c66cb
    return axios.request({
        url: "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address",
        method: "post",
        headers: {
            "Authorization": "Token 0d0d9ff7ec3ce2f8abb613d78394dd8dd83f6446",
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        data: data
    }).then(response => {
        if (response.status === 200) {
            return response.data;
        } else {
            return response.error;
        }
    });
}

function api(url, method, data, headers) {
    const PRODUCTION_URL = "https://rt-ds.dengisrazy.ru/v1/";
    const PRODUCTION_BASIC_AUTH = "Basic bW9iaWxlLWFwcDprZ0gzc3RnUk1DWXp5eFd1";
    const DEVELOPMENT_URL = "http://rc.rt-ds.dengisrazy.ru/v1/";
    const DEVELOPMENT_BASIC_AUTH = "Basic c2l0ZS1kczI6cDZ2OTVZNUxWRTQyYzVBWg==";

    let basicAuth;
    let baseUrl = "";

    switch (process.env.NODE_ENV) {
        case "development":
            baseUrl = DEVELOPMENT_URL;
            basicAuth = DEVELOPMENT_BASIC_AUTH;
            break;
        case "production" :
            baseUrl = PRODUCTION_URL;
            basicAuth = PRODUCTION_BASIC_AUTH;
            break;
    }

    return axios.request({
        url: baseUrl + url,
        method: method,
        headers: {"Authorization": basicAuth,'Content-Type': 'application/json', 'Accept': 'application/json'},
        data: data

    }).then(response => {
        if (response.status === 200) {
            return response.data;
        } else {
            return response.error;
        }
    });
}
