import FooterOtherPage from '../components/FooterOtherPage';
import HeaderContainer from "../wrappedComponents/HeaderContainer";
import React from "react";
// import init from "../init";

// interface StatlesRegPages<P = {}> extends React.SFC<P> {
//     getInitialProps?: (ctx: any) => Promise<P>
// }

export default function RegPages ({children}) {
// static getInitialProps({ctx}) {
//         init(ctx);
//
//         return ctx;
//     };
//     RegPages.getInitialProps = async ({children},ctx) => {
//         init(ctx);
//     };
    return (
        <>
            <HeaderContainer/>
            {children}
            <FooterOtherPage/>
        </>
    )
}
