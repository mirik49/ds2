import {connect} from 'react-redux'
import RestorePassword from "../js/components/RestorePassword";
import {clearError,setRestoreCode, setRestorePhone} from "../js/store/restore/actions";
import {restoreAuth, restorePassword} from "../js/store/restore/reducers";

const mapDispatchToProps = {
    setRestorePhone,
    clearError,
    restorePassword,
    setRestoreCode,
    restoreAuth
};

export default connect(state=>state, mapDispatchToProps)(RestorePassword);
