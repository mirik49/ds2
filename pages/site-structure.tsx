import React from "react";

import "../scss/components/site-structure.scss"
import Pages from "../js/layouts/Pages";
import Link from "next/link";

export default function SiteStructure() {

    return (
        <Pages>
            <section className="site-structure">
                <h1 className="site-structure__title">Структура сайта</h1>
                <ul className="site-structure__list-main">
                    <li className="site-structure__item-main">
                        <Link href="/">
                            <a className="site-structure__item-link"
                               id="main-page"
                            >Главная страница</a>
                        </Link>
                    </li>
                    <li className="site-structure__item-main">
                        <Link href="/about-company">
                            <a className="site-structure__item-link">О компании</a>
                        </Link>
                        <ul className="site-structure__list">
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">История становления</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Достижения и награды</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Документация</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Благотворительность</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Новости</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Новости</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Информация инвесторам</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Структура сайта</a>
                                </Link>
                            </li>

                        </ul>
                    </li>
                    <li className="site-structure__item-main">
                        <Link href="/">
                            <a className="site-structure__item-link">Клиентам</a>
                        </Link>
                        <ul className="site-structure__list">
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Оформить заявку</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Личный кабинет</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Вопросы и ответы</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Блог</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Отзывы клиентов</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Политика конфединциальности</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Города выдачи</a>
                                </Link>
                            </li>
                        </ul>
                    </li>
                    <li className="site-structure__item-main">
                        <Link href="/">
                            <a className="site-structure__item-link">Карьера</a>
                        </Link>
                        <ul className="site-structure__list">
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Вакансии</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">История успеха</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Корпоративный университет</a>
                                </Link>
                            </li>
                            <li className="site-structure__item">
                                <Link href="/">
                                    <a className="site-structure__item-link">Достижения HR</a>
                                </Link>
                            </li>
                        </ul>
                    </li>
                </ul>
            </section>
        </Pages>
    )
}

