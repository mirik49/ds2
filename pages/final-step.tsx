import {connect} from "react-redux";
import FinalStep from "../js/components/registration/FinalStep";
import {setPassword,setConfirmPassword,submitFinalStep} from "../js/store/registration/final-step/actions";

const mapDispatchToProps =
    {
        setPassword,
        setConfirmPassword,
        submitFinalStep
    };

export default connect(state=>state, mapDispatchToProps)(FinalStep);
