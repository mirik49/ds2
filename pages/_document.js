import Document, {Head, Html, Main, NextScript} from 'next/document'
// import Header from "../components/Head.tsx";
// import Footer from "../js/components/Footer.tsx";
import React from "react";
// import HeaderContainer from "../js/wrappedComponents/HeaderContainer";
// import cookie from 'react-cookies';
// import Router from 'next/router';

// import init from "../js/init";


class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx);
        // init(ctx);
        return {...initialProps}
    }

    render() {
        return (
            <Html lang="ru">
                <Head>
                    <title>Сайт деньги сразу</title>
                </Head>
                <body>
                <Main/>
                <NextScript/>
                {/*<Footer/>*/}
                </body>
            </Html>
        )
    }
}

export default MyDocument
