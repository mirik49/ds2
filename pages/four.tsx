import Four from "../js/components/registration/Four";
import {connect} from "react-redux";
import {clearErrorFour, setActivateCode,sentActivateCode, submitStepFour} from "../js/store/registration/step4/actions";

const mapDispatchToProps =
    {
        setActivateCode,
        submitStepFour,
        sentActivateCode,
        clearErrorFour

    };

export default connect(state=>state, mapDispatchToProps)(Four);
