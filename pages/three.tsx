import Three from "../js/components/registration/Three";
import {connect} from "react-redux";
import { clearError,
    runTestStep3, setAgreedWithBki, setAgreedWithDdo,
    setAmountPayMonthlyCredits, setAmountPayMonthlyLoans, setAmountTotalOtherObligations,
    setFirstGuarantName,
    setFirstGuarantTelephone,
    setHasOpenedCredits, setHasOpenedLoans,
    setIncomePerMonth, setIsPensioner, setIsUnemployed, setPhoneWorks, setPositionName,
    setSecondGuarantName,
    setSecondGuarantTelephone,
    setSelectedEducation,
    setSelectedGuarantorIncome,
    setSelectedGuarantorResidence,
    setSelectedLastWorkLength,
    setSelectedLoanPurpose,
    setSelectedMaritalStatus,
    setSkype, setWasDeclaredAsBankrupt, setWorkAddress, setWorkName,getDefaultDataStep3, submitStepThree
} from "../js/store/registration/step3/actions";

const mapDispatchToProps =
    {

        setSelectedGuarantorIncome,
        setSelectedGuarantorResidence,
        getDefaultDataStep3,
        setSelectedEducation,
        setSelectedMaritalStatus,
        setSelectedLastWorkLength,
        setSelectedLoanPurpose,
        setSkype,
        setFirstGuarantName,
        setFirstGuarantTelephone,
        setSecondGuarantName,
        setSecondGuarantTelephone,
        setIncomePerMonth,
        setHasOpenedCredits,
        setAmountPayMonthlyCredits,
        setHasOpenedLoans,
        setAmountPayMonthlyLoans,
        setAmountTotalOtherObligations,
        setIsPensioner,
        setIsUnemployed,
        setWorkName,
        setPositionName,
        setWorkAddress,
        setPhoneWorks,
        setWasDeclaredAsBankrupt,
        submitStepThree,
        runTestStep3,
        setAgreedWithDdo,
        setAgreedWithBki,
        clearError

    };

export default connect(state=>state, mapDispatchToProps)(Three);
