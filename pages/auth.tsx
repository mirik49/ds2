import {connect} from 'react-redux'
import Auth from "../js/components/Auth";
import {
    setAuthCode,
    setAuthPassword,
    setAuthPhone,
    clearError
} from "../js/store/auth/actions";
import {submitAuth} from "../js/store/auth/actions";

const mapDispatchToProps = {
    setAuthPhone,
    setAuthCode,
    submitAuth,
    clearError,
    setAuthPassword
};

export default connect(state=>state, mapDispatchToProps)(Auth);
